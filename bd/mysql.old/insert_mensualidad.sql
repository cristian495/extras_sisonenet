/*
-- Query: 
-- Date: 2017-10-01 23:33
*/
INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (1,1,'2017/09/30 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (2,1,'2017/10/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (3,1,'2017/11/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (4,1,'2017/12/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (5,1,'2018/01/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (6,1,'2018/02/28 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (7,1,'2018/03/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (8,1,'2018/04/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (9,1,'2018/05/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (10,1,'2018/06/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (11,1,'2018/07/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (12,1,'2018/08/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (13,2,'2017/10/25 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (14,2,'2017/11/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (15,2,'2017/12/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (16,2,'2018/01/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (17,2,'2018/02/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (18,2,'2018/03/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (19,2,'2018/04/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (20,2,'2018/05/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (21,2,'2018/06/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (22,2,'2018/07/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (23,2,'2018/08/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (24,2,'2018/09/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (25,3,'2017/06/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (26,3,'2017/07/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (27,3,'2017/08/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (28,3,'2017/09/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (29,3,'2018/10/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (30,3,'2018/11/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (31,3,'2018/12/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (32,3,'2018/01/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (33,3,'2018/02/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (34,3,'2018/03/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (35,3,'2018/04/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (36,3,'2018/05/18 00:09:13','anulado','gris');

