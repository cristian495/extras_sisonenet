CREATE  OR REPLACE VIEW onenetco_bdonenet.view_cobros_pendientes AS
SELECT c.idcontrato,c.estado as "contrato_estado",
m.idmensualidad,
cl.idcliente,
cl.nombre_razon,
m.num_cuota,
m.fecha_pago,
m.costo,
m.estado,
m.color_estado
-- now() as hoy 
FROM onenetco_bdonenet.mensualidad as m
INNER JOIN onenetco_bdonenet.contrato as c
ON m.idcontrato = c.idcontrato
INNER JOIN onenetco_bdonenet.cliente as cl
ON c.idcliente = cl.idcliente
-- WHERE 	m.fecha_pago >= CURDATE()  and
--		m.fecha_pago <= DATE_ADD(NOW(), INTERVAL 400 DAY)