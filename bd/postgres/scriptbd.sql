-- Database: bdonenet

-- -----------------------------------------------------
-- Table bdonenet.cliente
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.cliente (
  idcliente  SERIAL NOT NULL,
  nombre VARCHAR(45) NOT NULL ,
  apellido VARCHAR(100) NOT NULL ,
  dni VARCHAR(10) NOT NULL ,
  telefono VARCHAR(15) NOT NULL ,
  direccion VARCHAR(100) NOT NULL ,
  imagen_satelital VARCHAR(150) NULL ,
  referencia_direccion VARCHAR(150) NULL ,
  estado boolean NOT NULL DEFAULT true,
  PRIMARY KEY (idcliente) )



  -- -----------------------------------------------------
-- Table bdonenet.paquete
-- -----------------------------------------------------

CREATE  TABLE IF NOT EXISTS bdonenet.paquete (
  idpaquete SERIAL NOT NULL,
  nombre VARCHAR(100) NULL ,
  megas_subida DECIMAL(5,2) NULL ,
  megas_bajada DECIMAL(5,2) NULL ,
  precio_mensual DECIMAL(5,2) NOT NULL ,
  observacion VARCHAR(80) NULL ,
  estado BOOLEAN NOT NULL DEFAULT TRUE ,
  PRIMARY KEY (idpaquete) )



  -- -----------------------------------------------------
-- Table bdonenet.equipos
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.antena_emisora (
  idantena_emisora SERIAL NOT NULL,
  essid VARCHAR(150) NULL ,
  nombre VARCHAR(45) NULL ,
  marca VARCHAR(45) NULL ,
  modelo VARCHAR(45) NULL ,
  serie VARCHAR(45) NULL ,
  frecuencia VARCHAR(45) NULL ,
  ip VARCHAR(45) NULL ,
  mac VARCHAR(45) NULL ,
  usuario VARCHAR(45) NULL ,
  contrasenia VARCHAR(45) NULL ,
  estado BOOLEAN NOT NULL DEFAULT TRUE ,
  PRIMARY KEY (idantena_emisora) )




-- -----------------------------------------------------
-- Table bdonenet.contratos
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.contrato (
  idcontrato SERIAL NOT NULL,
  idcliente INT NOT NULL ,
  idpaquete INT   NULL ,
  idantena_emisora INT NOT NULL ,
  fecha_inicio_contrato TIMESTAMP NOT NULL ,
  duracion_contrato INT NOT NULL ,
  fecha_fin_contrato TIMESTAMP NOT NULL ,
  fecha_pago_servicio INT NOT NULL ,
  fecha_corte INT NOT NULL ,
  costo_instalacion DECIMAL(7,2) NOT NULL ,
  costo_ap DECIMAL(7,2) NOT NULL ,
  tipo_pago_ap VARCHAR(9) NOT NULL ,
  marca_ap VARCHAR(45) NULL ,
  modelo_ap VARCHAR(45) NULL ,  
  serie_ap VARCHAR(45) NULL , 
  mac_ap VARCHAR(27) NULL ,
  ip_ap VARCHAR(20) NULL ,
  usuario_ap VARCHAR(15) NULL ,
  contrasenia_ap VARCHAR(40) NULL ,
  marca_router VARCHAR(45) NULL ,
  modelo_router VARCHAR(45) NULL ,
  serie_router VARCHAR(45) NULL ,
  mac_router VARCHAR(27) NULL ,
  ip_router VARCHAR(20) NULL ,
  usuario_router VARCHAR(15) NULL ,
  contrasenia_router VARCHAR(40) NULL ,
  
  estado BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY (idcontrato) ,
  CONSTRAINT idcliente
    FOREIGN KEY (idcliente)
    REFERENCES bdonenet.cliente (idcliente)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT idpaquete
    FOREIGN KEY (idpaquete)
    REFERENCES bdonenet.paquete (idpaquete)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT idantena_emisora
    FOREIGN KEY (idantena_emisora)
    REFERENCES bdonenet.antena_emisora (idantena_emisora)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


-- -----------------------------------------------------
-- Table bdonenet.mensualidad
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.mensualidad (
  idmensualidad SERIAL  NOT NULL ,
  idcontrato INT NOT NULL ,
  num_cuota INT NOT NULL ,
  costo DECIMAL(7,2) NOT NULL ,
  fecha_pago TIMESTAMP NOT NULL,
  estado VARCHAR(20) NOT NULL ,
  color_estado VARCHAR(20) NOT NULL ,
  PRIMARY KEY (idmensualidad) ,
  CONSTRAINT idcontrato
    FOREIGN KEY (idcontrato)
    REFERENCES bdonenet.contrato (idcontrato)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)



-- -----------------------------------------------------
-- Table bdonenet.pago_mensualidad
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.pago_mensualidad (
  idpago_mensualidad SERIAL NOT NULL ,
  idcliente INT NOT NULL ,
  fecha_hora_pagado TIMESTAMP NOT NULL ,
  tipo_comprobante VARCHAR(20) NOT NULL ,
  serie_comprobante VARCHAR(7) NULL ,
  num_comprobante VARCHAR(15) NOT NULL ,
  mora DECIMAL(5,2) NULL DEFAULT 0.00,
  total_pago DECIMAL(5,2) NOT NULL,
  -- impuesto DECIMAL(5,2) NULL DEFAULT 0.00,
  costo_mensualidad DECIMAL(7,2) NULL ,
  estado VARCHAR(15) NOT NULL ,
  PRIMARY KEY (idpago_mensualidad) ,
  CONSTRAINT fk_pago_mensualidad_cliente
    FOREIGN KEY (idcliente)
    REFERENCES  bdonenet.cliente (idcliente)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


-- -----------------------------------------------------
-- Table bdonenet.detalle_pago
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.detalle_pago (
  iddetalle_pago SERIAL  NOT NULL,
  idpago_mensualidad INT NOT NULL ,
  idmensualidad INT NOT NULL ,
  -- total DECIMAL(5,2) NULL ,
  -- monto_pagado DECIMAL(5,2) NOT NULL ,
  PRIMARY KEY (iddetalle_pago) ,
  CONSTRAINT idpago_mensualidad
    FOREIGN KEY (idpago_mensualidad)
    REFERENCES bdonenet.pago_mensualidad (idpago_mensualidad)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT idmensualidad
    FOREIGN KEY (idmensualidad)
    REFERENCES bdonenet.mensualidad (idmensualidad)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


-- -----------------------------------------------------
-- Table bdonenet.users
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.users (
  id SERIAL  NOT NULL,
  idcliente INT NULL,
  tipo_usuario SMALLINT,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NULL,
  password VARCHAR(255) NOT NULL,
  remember_token VARCHAR(100) NULL,
  created_at TIMESTAMP NULL,
  updated_at TIMESTAMP NULL,
  estado BOOLEAN NOT NULL DEFAULT TRUE,

  PRIMARY KEY (id) ,
  CONSTRAINT idcliente
    FOREIGN KEY (idcliente)
    REFERENCES bdonenet.cliente (idcliente)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)




    -- -----------------------------------------------------
-- Table bdonenet.password_resets
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS bdonenet.password_resets (
  email VARCHAR(255) NULL,
  token VARCHAR(100) NULL,
  created_at TIMESTAMP NULL)