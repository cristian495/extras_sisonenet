CREATE  OR REPLACE VIEW bdonenet.view_cobros_pendientes AS
SELECT c.idcontrato,
m.idmensualidad,
cl.idcliente,
cl.nombre,
cl.apellido,
m.num_cuota,
m.fecha_pago,
m.costo,
m.estado,
m.color_estado
-- now() as hoy 
FROM bdonenet.mensualidad as m
INNER JOIN bdonenet.contrato as c
ON m.idcontrato = c.idcontrato
INNER JOIN bdonenet.cliente as cl
ON c.idcliente = cl.idcliente
-- WHERE 	m.fecha_pago >= CURDATE()  and
--		m.fecha_pago <= DATE_ADD(NOW(), INTERVAL 400 DAY)
