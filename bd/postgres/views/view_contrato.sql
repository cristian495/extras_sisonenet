CREATE VIEW bdonenet2.view_contrato AS
SELECT c.idcontrato as id,
cl.idcliente as "idcliente",
cl.nombre as "nombre_cliente",
cl.apellido as "apellido_cliente",
cl.dni as "dni",
cl.telefono as "telefono",
cl.tipo_via as "tipo_via",
cl.nombre_via  as "nombre_via",
cl.numero_vivienda  as "numero_vivienda",
cl.tipo_zona  as "tipo_zona",
cl.nombre_zona  as "nombre_zona",
cl.estado as "estado_cliente",
p.idpaquete "idpaquete",
p.nombre "paquete",
p.precio_mensual "precio_mensual",
p.megas_bajada "megas_bajada",
p.megas_subida "megas_subida",
c.fecha_pago_servicio "fecha_pago",
c.fecha_corte "fecha_corte",
c.fecha_inicio_contrato "fecha_inicio_contrato",
c.duracion_contrato "duracion_contrato",
c.fecha_fin_contrato "fecha_fin_contrato",
a.idantena_emisora as "idantena_emisora",
a.essid as "antena_essid",
a.mac as "antena_mac",
a.ip as "antena_ip",
c.marca_ap as "marca_ap",
c.modelo_ap as "modelo_ap",
c.serie_ap as "serie_ap",
c.marca_router as "marca_router",
c.modelo_router as "modelo_router",
c.serie_router as "serie_router",
c.costo_instalacion as "costo_instalacion",
c.costo_ap as "costo_ap",
c.tipo_pago_ap as "tipo_pago_ap",
c.estado 
FROM bdonenet.contrato as c 
INNER JOIN bdonenet.cliente as cl
ON c.idcliente = cl.idcliente
INNER JOIN bdonenet.paquete as p
ON c.idpaquete = p.idpaquete
INNER JOIN bdonenet.antena_emisora as a
ON  c.idantena_emisora = a.idantena_emisora 