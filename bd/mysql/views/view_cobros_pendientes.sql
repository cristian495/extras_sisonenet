CREATE  OR REPLACE VIEW bdonenet2.view_cobros_pendientes AS
SELECT c.idcontrato,c.estado as "contrato_estado",
m.idmensualidad,
cl.idcliente,
cl.nombre_razon,
m.num_cuota,
m.fecha_pago,
m.costo,
m.estado,
m.color_estado
-- now() as hoy 
FROM bdonenet2.mensualidad as m
INNER JOIN bdonenet2.contrato as c
ON m.idcontrato = c.idcontrato
INNER JOIN bdonenet2.cliente as cl
ON c.idcliente = cl.idcliente
-- WHERE 	m.fecha_pago >= CURDATE()  and
--		m.fecha_pago <= DATE_ADD(NOW(), INTERVAL 400 DAY)