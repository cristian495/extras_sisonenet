CREATE VIEW bdonenet2.view_pagos AS
SELECT  
  pm.idpago_mensualidad,

  c.idcliente,
  c.nombre_razon,
  c.dni,
  c.telefono,
  c.tipo_via,
  c.nombre_via,
  c.numero_vivienda,
  c.tipo_zona,
  c.nombre_zona,

 
 pm.fecha_hora_pagado,
 pm.tipo_comprobante,
 pm.serie_comprobante,
 pm.num_comprobante,
 -- pm.costo_mensualidad,


 m.idcontrato,
 m.num_cuota,
 m.costo, 
 m.fecha_pago,
-- sum(m.costo) as total

con.estado as estado_contrato



FROM bdonenet2.detalle_pago as dp
INNER JOIN bdonenet2.mensualidad as m
ON dp.idmensualidad = m.idmensualidad
INNER JOIN bdonenet2.pago_mensualidad as pm
ON dp.idpago_mensualidad = pm.idpago_mensualidad 
INNER JOIN bdonenet2.cliente  AS c
ON pm.idcliente = c.idcliente 
INNER JOIN bdonenet2.contrato AS con
ON m.idcontrato = con.idcontrato

-- OBTIENE EL PAGO Y SU TOTAL
-- GROUP BY pm.idpago_mensualidad,c.idcliente,c.nombre,m.costo
