-- ANTENA EMISORA
INSERT INTO `antena_emisora` (`idantena_emisora`,`essid`,`nombre`,`marca`,`modelo`,`serie`,`frecuencia`,`ip`,`mac`,`usuario`,`contrasenia`) 
VALUES (1,'internet 40 soles #91828332','ubiquiti01','ubiquiti','sectorial','2343231','5mhz','192.168.11.1','23:d3:f0:33:34:01','ubnt','ubnt');

INSERT INTO `antena_emisora` (`idantena_emisora`,`essid`,`nombre`,`marca`,`modelo`,`serie`,`frecuencia`,`ip`,`mac`,`usuario`,`contrasenia`) 
VALUES (2,'internet victorai 50 soles#93728283','tplink antena02','tplink','omnidireccional','3424231a','2.4mhz','192.168.22.1','22:32:ff:a4:44:23','admin','admin@123');

-- CLIENTE
INSERT INTO `cliente` (`idcliente`,`nombre_razon`,`apellido`,`dni`,`telefono`,`email`,`direccion`,`imagen_satelital`,`referencia_direccion`) VALUES (1,'cristian','cruz benel','73766308','978143031','cristian@gmail.com','villas de la ensenada mz c lote 01','/clientes/cristian/foto.jpg','por la universidad señor de sipan');
INSERT INTO `cliente` (`idcliente`,`nombre`,`apellido`,`dni`,`telefono`,`email`,`direccion`,`imagen_satelital`,`referencia_direccion`) VALUES (2,'Miguel','Oblitas Jimenez','14564855','998800769','jose@gmail.com','la primavera belaunde y valdivieso','/clientes/miguel/foto.jpg','por totus la primavera');
INSERT INTO `cliente` (`idcliente`,`nombre`,`apellido`,`dni`,`telefono`,`email`,`direccion`,`imagen_satelital`,`referencia_direccion`) VALUES (3,'Marco','Sanchez Rivas','34321232','908328281','marco@gmail.com','simon bolivar','/clientes/marco/foto.jpg','por simo bolivar');


-- PAQUETE
INSERT INTO `paquete` (`idpaquete`,`nombre`,`megas_subida`,`megas_bajada`,`precio_mensual`,`observacion`,`estado`) 
VALUES (1,'economico',2.00,1.00,40.00,NULL,1);

INSERT INTO `paquete` (`idpaquete`,`nombre`,`megas_subida`,`megas_bajada`,`precio_mensual`,`observacion`,`estado`) 
VALUES (2,'standar',4.00,2.00,60.00,NULL,1);

INSERT INTO `paquete` (`idpaquete`,`nombre`,`megas_subida`,`megas_bajada`,`precio_mensual`,`observacion`,`estado`) 
VALUES (3,'premium',12.00,3.8,120.00,NULL,1);

-- CONTRATO
INSERT INTO `contrato` (`idcontrato`,`idcliente`,`idpaquete`,`idantena_emisora`,`fecha_inicio_contrato`,`fecha_fin_contrato`,`fecha_pago_servicio`,`costo_instalacion`,`marca_ap`,`modelo_ap`,`serie_ap`,`marca_router`,`modelo_router`,`serie_router`,`estado`) 
VALUES (1,1,2,1,'2017/09/30','2018/08/30','2017/09/30',180.00,'ubiquiti','direccional rejilla','dd2342a','tplink','322h','afe234b',true);

INSERT INTO `contrato` (`idcontrato`,`idcliente`,`idpaquete`,`idantena_emisora`,`fecha_inicio_contrato`,`fecha_fin_contrato`,`fecha_pago_servicio`,`costo_instalacion`,`marca_ap`,`modelo_ap`,`serie_ap`,`marca_router`,`modelo_router`,`serie_router`,`estado`) 
VALUES (2,3,1,2,'2017/10/25','2018/09/25','2017/10/25',180.00,'ubiquiti','direccional plato','daf2321','tplink','322h','sls23sd',true);

INSERT INTO `contrato` (`idcontrato`,`idcliente`,`idpaquete`,`idantena_emisora`,`fecha_inicio_contrato`,`fecha_fin_contrato`,`fecha_pago_servicio`,`costo_instalacion`,`marca_ap`,`modelo_ap`,`serie_ap`,`marca_router`,`modelo_router`,`serie_router`,`estado`) 
VALUES (3,2,3,1,'2017/06/18','2018/05/18','2017/06/18',180.00,'ubiquiti','direccional rejilla','as2312d','tplink','322h','ewd23g',false);



-- MENSUALIDAD
INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (1,1,'2017/09/30 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (2,1,'2017/10/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (3,1,'2017/11/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (4,1,'2017/12/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (5,1,'2018/01/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (6,1,'2018/02/28 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (7,1,'2018/03/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (8,1,'2018/04/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (9,1,'2018/05/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (10,1,'2018/06/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (11,1,'2018/07/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (12,1,'2018/08/30 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (13,2,'2017/10/25 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (14,2,'2017/11/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (15,2,'2017/12/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (16,2,'2018/01/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (17,2,'2018/02/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (18,2,'2018/03/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (19,2,'2018/04/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (20,2,'2018/05/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (21,2,'2018/06/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (22,2,'2018/07/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (23,2,'2018/08/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (24,2,'2018/09/25 00:09:13','pendiente','negro');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (25,3,'2017/06/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (26,3,'2017/07/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (27,3,'2017/08/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (28,3,'2017/09/18 00:09:13','pagado','verde');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (29,3,'2018/10/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (30,3,'2018/11/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (31,3,'2018/12/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (32,3,'2018/01/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (33,3,'2018/02/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (34,3,'2018/03/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (35,3,'2018/04/18 00:09:13','anulado','gris');

INSERT INTO `mensualidad` (`idmensualidad`,`idcontrato`,`fecha_pago`,`estado`,`color_estado`) 
VALUES (36,3,'2018/05/18 00:09:13','anulado','gris');



-- PAGO MENSUALIDAD
INSERT INTO `pago_mensualidad` (`idpago_mensualidad`,`idcliente`,`fecha_hora_pagado`,`tipo_comprobante`,`serie_comprobante`,`num_comprobante`,`impuesto`,`costo_total`,`estado`) 
VALUES (1,1,'2017/09/30 10:10:00','boleta','23423s','324323s',0,60,'pagado');

INSERT INTO `pago_mensualidad` (`idpago_mensualidad`,`idcliente`,`fecha_hora_pagado`,`tipo_comprobante`,`serie_comprobante`,`num_comprobante`,`impuesto`,`costo_total`,`estado`) 
VALUES (2,3,'2017/10/25 15:14:52','boleta','15515','2113131',0,40,'pagado');

INSERT INTO `pago_mensualidad` (`idpago_mensualidad`,`idcliente`,`fecha_hora_pagado`,`tipo_comprobante`,`serie_comprobante`,`num_comprobante`,`impuesto`,`costo_total`,`estado`) 
VALUES (3,2,'2017/06/18 09:50:12','boleta','51465','846+8685',0,240,'pagado');

INSERT INTO `pago_mensualidad` (`idpago_mensualidad`,`idcliente`,`fecha_hora_pagado`,`tipo_comprobante`,`serie_comprobante`,`num_comprobante`,`impuesto`,`costo_total`,`estado`) 
VALUES (4,2,'2017/08/16 18:08:11','boleta','89765','876867668',0,120,'pagado');

INSERT INTO `pago_mensualidad` (`idpago_mensualidad`,`idcliente`,`fecha_hora_pagado`,`tipo_comprobante`,`serie_comprobante`,`num_comprobante`,`impuesto`,`costo_total`,`estado`) 
VALUES (5,2,'2017/09/17 16:12:07','boleta','5351648','86846844',0,120,'pagado');




-- DETALLE PAGO MENSUALIDAD
INSERT INTO `detalle_pago` (`iddetalle_pago`,`idpago_mensualidad`,`idmensualidad`,`monto_interes`,`monto_pagado`) 
VALUES (1,1,1,0,60);

INSERT INTO `detalle_pago` (`iddetalle_pago`,`idpago_mensualidad`,`idmensualidad`,`monto_interes`,`monto_pagado`) 
VALUES (2,2,13,0,40);

INSERT INTO `detalle_pago` (`iddetalle_pago`,`idpago_mensualidad`,`idmensualidad`,`monto_interes`,`monto_pagado`) 
VALUES (3,3,25,0,120);

INSERT INTO `detalle_pago` (`iddetalle_pago`,`idpago_mensualidad`,`idmensualidad`,`monto_interes`,`monto_pagado`) 
VALUES (4,3,26,0,120);

INSERT INTO `detalle_pago` (`iddetalle_pago`,`idpago_mensualidad`,`idmensualidad`,`monto_interes`,`monto_pagado`) 
VALUES (5,4,27,0,120);

INSERT INTO `detalle_pago` (`iddetalle_pago`,`idpago_mensualidad`,`idmensualidad`,`monto_interes`,`monto_pagado`) 
VALUES (6,5,28,0,120);


