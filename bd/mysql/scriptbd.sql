SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `bdonenet2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bdonenet2` ;

-- -----------------------------------------------------
-- Table `bdonenet2`.`cliente`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bdonenet2`.`cliente` (
  `idcliente`  INT NOT NULL AUTO_INCREMENT,
  `tipo_persona` VARCHAR(10) NULL ,
  `nombre_razon` VARCHAR(45) NOT NULL ,
  -- `apellido` VARCHAR(100) NOT NULL ,
  `dni` VARCHAR(10) NOT NULL ,
  `telefono` VARCHAR(15) NOT NULL ,
  `telefono_adicional` VARCHAR(15) NOT NULL ,
  `tipo_via` VARCHAR(30) NOT NULL ,
  `nombre_via` VARCHAR(30) NOT NULL ,
  `numero_vivienda` VARCHAR(8) NOT NULL ,
  `tipo_zona` VARCHAR(30) NOT NULL ,
  `nombre_zona` VARCHAR(30) NOT NULL ,
  `departamento` VARCHAR(30) NOT NULL ,
  `provincia` VARCHAR(30) NOT NULL ,
  `distrito` VARCHAR(30) NOT NULL ,
  `referencia_direccion` VARCHAR(250) NULL ,
  `imagen_satelital` VARCHAR(200) NULL ,
 
  `estado` boolean NOT NULL DEFAULT 1,
  `en_contrato` boolean NOT NULL DEFAULT 0,


  PRIMARY KEY (`idcliente`) 
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bdonenet2`.`paquete`
-- -----------------------------------------------------

CREATE  TABLE IF NOT EXISTS `bdonenet2`.`paquete` (
  `idpaquete` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL ,
  `megas_subida` DECIMAL(5,2) NULL ,
  `megas_bajada` DECIMAL(5,2) NULL ,
  `megas_subida_comercial` DECIMAL(5,2) NULL ,
  `megas_descarga_comercial` DECIMAL(5,2) NULL ,
  `precio_mensual` DECIMAL(5,2) NOT NULL ,
  `observacion` VARCHAR(80) NULL ,
  `estado` BOOLEAN NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`idpaquete`) 
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bdonenet2`.`equipos`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bdonenet2`.`antena_emisora` (
  `idantena_emisora` INT NOT NULL AUTO_INCREMENT,
  `essid` VARCHAR(150) NULL ,
  `nombre` VARCHAR(45) NULL ,
  `marca` VARCHAR(45) NULL ,
  `modelo` VARCHAR(45) NULL ,
  `serie` VARCHAR(45) NULL ,
  `frecuencia` VARCHAR(45) NULL ,
  `ip` VARCHAR(45) NULL ,
  `mac` VARCHAR(45) NULL ,
  `usuario` VARCHAR(45) NULL ,
  `contrasenia` VARCHAR(45) NULL ,
  `estado` BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY (`idantena_emisora`)
   )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bdonenet2`.`contratos`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bdonenet2`.`contrato` (
  `idcontrato` INT NOT NULL AUTO_INCREMENT,
  `idcliente` INT NOT NULL ,
  `idpaquete` INT NOT NULL ,
  `idantena_emisora` INT  NULL ,
  `fecha_inicio_contrato` DATETIME NOT NULL ,
  `duracion_contrato` INT(2) NOT NULL ,
  `fecha_fin_contrato` DATETIME NOT NULL ,
  `fecha_pago_servicio` INT(2) NOT NULL ,
  `fecha_corte` TINYINT(2) NOT NULL ,
  `costo_instalacion` DECIMAL(7,2) NOT NULL ,
  `costo_ap` DECIMAL(7,2) NOT NULL ,
  `costo_ap_mensualmente` DECIMAL(7,2) NOT NULL ,
  `tipo_pago_ap` VARCHAR(9) NOT NULL ,
  `marca_ap` VARCHAR(45) NULL ,
  `modelo_ap` VARCHAR(45) NULL ,  
  `serie_ap` VARCHAR(45) NULL , 
  `mac_ap` VARCHAR(27) NULL ,
  `ip_ap` VARCHAR(20) NULL ,
  `usuario_ap` VARCHAR(15) NULL ,
  `contrasenia_ap` VARCHAR(40) NULL ,
  `marca_router` VARCHAR(45) NULL ,
  `modelo_router` VARCHAR(45) NULL ,
  `serie_router` VARCHAR(45) NULL ,
  `mac_router` VARCHAR(27) NULL ,
  `ip_router` VARCHAR(20) NULL ,
  `usuario_router` VARCHAR(15) NULL ,
  `contrasenia_router` VARCHAR(40) NULL ,
  
  `estado` TINYINT(1) NOT NULL DEFAULT TRUE,
  PRIMARY KEY (`idcontrato`) ,
  INDEX `idcliente_idx` (`idcliente` ASC) ,
  INDEX `idpaquete_idx` (`idpaquete` ASC) ,
  INDEX `idantena_emisora_idx` (`idantena_emisora` ASC) ,
  CONSTRAINT `idcliente`
    FOREIGN KEY (`idcliente` )
    REFERENCES `bdonenet2`.`cliente` (`idcliente` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idpaquete`
    FOREIGN KEY (`idpaquete` )
    REFERENCES `bdonenet2`.`paquete` (`idpaquete` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idantena_emisora`
    FOREIGN KEY (`idantena_emisora` )
    REFERENCES `bdonenet2`.`antena_emisora` (`idantena_emisora` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    
    
    
    )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bdonenet2`.`mensualidad`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bdonenet2`.`mensualidad` (
  `idmensualidad` INT NOT NULL AUTO_INCREMENT,
  `idcontrato` INT NOT NULL ,
  `num_cuota` INT(2) NOT NULL ,
  `costo` DECIMAL(7,2) NOT NULL ,
  `fecha_pago` DATETIME NOT NULL,
  `estado` VARCHAR(20) NOT NULL ,
  `color_estado` VARCHAR(20) NOT NULL ,
  PRIMARY KEY (`idmensualidad`) ,
  INDEX `idcontrato_idx` (`idcontrato` ASC) ,
  CONSTRAINT `idcontrato`
    FOREIGN KEY (`idcontrato` )
    REFERENCES `bdonenet2`.`contrato` (`idcontrato` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bdonenet2`.`pago_mensualidad`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bdonenet2`.`pago_mensualidad` (
  `idpago_mensualidad` INT NOT NULL AUTO_INCREMENT,
  `idcliente` INT NOT NULL ,
  `fecha_hora_pagado` DATETIME NOT NULL ,
  `tipo_comprobante` VARCHAR(20) NOT NULL ,
  `serie_comprobante` VARCHAR(10) NULL ,
  `num_comprobante` VARCHAR(15) NOT NULL ,
  `mora` DECIMAL(5,2) NULL DEFAULT 0.00,
  `total_pago` DECIMAL(5,2) NOT NULL,
-- impuesto DECIMAL(5,2) NULL DEFAULT 0.00,
  `costo_mensualidad` DECIMAL(5,2) NULL ,
  `estado` VARCHAR(10) NOT NULL ,

  PRIMARY KEY (`idpago_mensualidad`) ,
  INDEX `fk_pago_mensualidad_cliente1_idx` (`idcliente` ASC) ,
  CONSTRAINT `fk_pago_mensualidad_cliente`
    FOREIGN KEY (`idcliente` )
    REFERENCES `bdonenet2`.`cliente` (`idcliente` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bdonenet2`.`detalle_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdonenet2`.`detalle_pago` (
  `iddetalle_pago` INT NOT NULL AUTO_INCREMENT,
  `idpago_mensualidad` INT NOT NULL ,
  `idmensualidad` INT NOT NULL ,
  PRIMARY KEY (`iddetalle_pago`),
  INDEX `idpago_mensualidad_idx` (`idpago_mensualidad` ASC),
  INDEX `idmensualidad_idx` (`idmensualidad` ASC),
  CONSTRAINT `idpago_mensualidad`
    FOREIGN KEY (`idpago_mensualidad` )
    REFERENCES `bdonenet2`.`pago_mensualidad` (`idpago_mensualidad` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idmensualidad`
    FOREIGN KEY (`idmensualidad` )
    REFERENCES `bdonenet2`.`mensualidad` (`idmensualidad` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table bdonenet2.users
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bdonenet2`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idcliente` INT NULL,
  `tipo_usuario` TINYINT,
  `name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(255) NOT NULL,
  `remember_token` VARCHAR(100) NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  `estado` BOOLEAN NOT NULL DEFAULT TRUE,

  PRIMARY KEY (`id`) ,
  CONSTRAINT `idcliente_fk`
    FOREIGN KEY (`idcliente`)
    REFERENCES `bdonenet2`.`cliente` (`idcliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table bdonenet2.password_resets
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdonenet2`.`password_resets` (
  `email` VARCHAR(255) NULL,
  `token` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table bdonenet2.publicidad
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bdonenet2`.`publicidad`(
  `id` INT  NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(200) NULL,
  `tipo_imagen` VARCHAR(8) NOT NULL,
  `ruta_imagen` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`) 
)
ENGINE = InnoDB;

USE `bdonenet2` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


