-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-02-2018 a las 12:52:17
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdonenet2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antena_emisora`
--

CREATE TABLE `antena_emisora` (
  `idantena_emisora` int(11) NOT NULL,
  `essid` varchar(150) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `serie` varchar(45) DEFAULT NULL,
  `frecuencia` varchar(45) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `mac` varchar(45) DEFAULT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `contrasenia` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `antena_emisora`
--

INSERT INTO `antena_emisora` (`idantena_emisora`, `essid`, `nombre`, `marca`, `modelo`, `serie`, `frecuencia`, `ip`, `mac`, `usuario`, `contrasenia`, `estado`) VALUES
(1, 'prueba', 'prueba', 'prueba', 'prueba', '23423', '5 Ghz', 'se', 'aser', 'rae', 'ase21', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `tipo_persona` varchar(10) DEFAULT NULL,
  `nombre_razon` varchar(50) NOT NULL,
  `dni` varchar(10) NOT NULL,
  `tipo_via` varchar(30) NOT NULL,
  `nombre_via` varchar(30) NOT NULL,
  `numero_vivienda` varchar(8) NOT NULL,
  `tipo_zona` varchar(30) NOT NULL,
  `nombre_zona` varchar(30) NOT NULL,
  `departamento` varchar(30) DEFAULT NULL,
  `provincia` varchar(30) DEFAULT NULL,
  `distrito` varchar(30) DEFAULT NULL,
  `telefono` varchar(15) NOT NULL,
  `telefono_adicional` varchar(15) DEFAULT NULL,
  `imagen_satelital` varchar(200) DEFAULT NULL,
  `referencia_direccion` varchar(250) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `tipo_persona`, `nombre_razon`, `dni`, `tipo_via`, `nombre_via`, `numero_vivienda`, `tipo_zona`, `nombre_zona`, `departamento`, `provincia`, `distrito`, `telefono`, `telefono_adicional`, `imagen_satelital`, `referencia_direccion`, `estado`) VALUES
(1, '', 'crstian paquete', '123456', '', '', '', '', '', NULL, NULL, NULL, '55555', NULL, NULL, NULL, 1),
(2, '', 'crstian paquete', '5555', '', '', '', '', '', NULL, NULL, NULL, '555', NULL, NULL, NULL, 1),
(3, '', 'gftwe', '324', '', '', '', '', '', NULL, NULL, NULL, '234', NULL, NULL, NULL, 1),
(4, '', 'sdfa', '324', '', '', '', '', '', NULL, NULL, NULL, '234', NULL, NULL, NULL, 1),
(5, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(6, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(7, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(8, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(9, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(10, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(11, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(12, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(13, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(14, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(15, '', 'cristian arturo cruz benel', '73766308', '', '', '', '', '', NULL, NULL, NULL, '978143031', NULL, NULL, NULL, 1),
(16, '', 'dsg', '234', '', '', '', '', '', NULL, NULL, NULL, '23423', NULL, NULL, NULL, 1),
(17, '', 'jose miguel orlando oblitas jimenez', '73766308', '', '', '', '', '', NULL, NULL, NULL, '998800769', '978143031', NULL, NULL, 1),
(18, '', 'jose miguel orlando', '73766308', 'parque', 'sin nombre', '234', 'cooperativa', 'villas de la ensenada', 'lambayeque', 'chiclayo', 'chiclayo', '978143031', '978529057', NULL, 'polleria campos', 1),
(19, '', 'cristian arturo', '737663087', 'paseo', 'avenida belaunde', '234', 'unidad vecinal', 'villas de la ensenada', NULL, NULL, NULL, '978143031', '978529057', NULL, NULL, 1),
(20, 'persona', 'cristian arturo', '737663088', 'avenida', 'sin nombre', '234', 'conjunto habitacional', 'sfad', NULL, NULL, NULL, '978143031', '9829057', NULL, NULL, 1),
(21, 'persona', 'cristian arturo', '737663088', 'avenida', 'sin nombre', '234', 'conjunto habitacional', 'sfad', NULL, NULL, NULL, '978143031', '9829057', NULL, NULL, 1),
(22, 'persona', 'teonila', '16571214', 'avenida', 'zarumilla', '321', 'urbanizacion', 'los ingenierios', NULL, NULL, NULL, '978529057', '978143031', NULL, NULL, 1),
(24, NULL, 'FASD', '234355577', 'avenida', 'sin nombre', 'fsda', 'urbanizacion', 'sfad', 'lambayeque', 'ferreñafe', 'incahuasi', '234', '234', NULL, 'por universidad señor de sipan', 1),
(25, NULL, 'erqe', '2342', 'portal', 'sin nombre', '234', 'residencial', 'villas de la ensenada', 'lambayeque', 'chiclayo', 'chongoyape', '23423', '32423', NULL, 'por universidad señor de sipan', 1),
(26, NULL, 'maria alcira sanchez muñoz', '8888', 'avenida', 'zarumilla', '322', 'urbanizacion', 'los alamos', 'lambayeque', 'chiclayo', 'chiclayo', '2382308', '8230230', NULL, 'por universidad señor de sipan', 1),
(27, 'persona', '345 erswt ert', '345', 'avenida', 'l', 'fsda', 'conjunto habitacional', 'sfad', 'lambayeque', 'chiclayo', 'saña', '23423', '978529057', NULL, 'por universidad señor de sipan', 1),
(28, 'persona', '345 erswt ert', '345', 'avenida', 'l', 'fsda', 'conjunto habitacional', 'sfad', 'lambayeque', 'chiclayo', 'saña', '23423', '978529057', NULL, 'por universidad señor de sipan', 1),
(29, 'persona', 'sdfa sdf sdf', '2322', 'paseo', 'sdfa', '234', 'residencial', '3234', 'lambayeque', 'chiclayo', 'patapo', 'dfsa', 'sdfa', NULL, 'por colegio juan tomis stack', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE `contrato` (
  `idcontrato` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `idpaquete` int(11) NOT NULL,
  `idantena_emisora` int(11) DEFAULT NULL,
  `fecha_inicio_contrato` datetime NOT NULL,
  `duracion_contrato` int(2) NOT NULL,
  `fecha_fin_contrato` datetime NOT NULL,
  `fecha_pago_servicio` varchar(40) NOT NULL,
  `fecha_corte` varchar(40) NOT NULL,
  `costo_instalacion` decimal(7,2) NOT NULL,
  `costo_ap` decimal(7,2) NOT NULL,
  `costo_ap_mensualmente` decimal(7,2) NOT NULL,
  `tipo_pago_ap` varchar(9) NOT NULL,
  `marca_ap` varchar(45) DEFAULT NULL,
  `modelo_ap` varchar(45) DEFAULT NULL,
  `serie_ap` varchar(45) DEFAULT NULL,
  `mac_ap` varchar(27) DEFAULT NULL,
  `ip_ap` varchar(20) DEFAULT NULL,
  `usuario_ap` varchar(15) DEFAULT NULL,
  `contrasenia_ap` varchar(40) DEFAULT NULL,
  `marca_router` varchar(45) DEFAULT NULL,
  `modelo_router` varchar(45) DEFAULT NULL,
  `serie_router` varchar(45) DEFAULT NULL,
  `mac_router` varchar(27) DEFAULT NULL,
  `ip_router` varchar(20) DEFAULT NULL,
  `usuario_router` varchar(15) DEFAULT NULL,
  `contrasenia_router` varchar(40) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contrato`
--

INSERT INTO `contrato` (`idcontrato`, `idcliente`, `idpaquete`, `idantena_emisora`, `fecha_inicio_contrato`, `duracion_contrato`, `fecha_fin_contrato`, `fecha_pago_servicio`, `fecha_corte`, `costo_instalacion`, `costo_ap`, `costo_ap_mensualmente`, `tipo_pago_ap`, `marca_ap`, `modelo_ap`, `serie_ap`, `mac_ap`, `ip_ap`, `usuario_ap`, `contrasenia_ap`, `marca_router`, `modelo_router`, `serie_router`, `mac_router`, `ip_router`, `usuario_router`, `contrasenia_router`, `estado`) VALUES
(1, 1, 1, 1, '2017-11-15 19:05:02', 1, '2018-11-15 19:05:02', '15', '16', '1.00', '300.00', '0.00', 'contado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(2, 8, 3, NULL, '2017-12-22 07:24:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 9, 3, NULL, '2017-12-05 00:00:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(4, 10, 3, NULL, '2017-12-05 00:00:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(5, 11, 3, NULL, '2017-12-05 00:00:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(6, 12, 3, NULL, '2017-12-05 00:00:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(7, 13, 3, NULL, '2017-12-05 00:00:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(8, 14, 1, NULL, '2017-12-05 00:00:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(9, 15, 1, NULL, '2017-12-05 00:00:00', 1, '2018-12-05 00:00:00', '5 de cada mes', '6 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(10, 16, 1, NULL, '2017-12-06 00:00:00', 2, '2019-12-06 00:00:00', '6 de cada mes', '7 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(11, 17, 2, NULL, '2017-12-10 00:00:00', 2, '2019-12-10 00:00:00', '10 de cada mes', '11 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(12, 18, 2, NULL, '2017-12-10 00:00:00', 1, '2018-12-10 00:00:00', '10 de cada mes', '11 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, 19, 2, NULL, '2017-12-11 00:00:00', 1, '2018-12-11 00:00:00', '11 de cada mes', '12 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(14, 20, 1, NULL, '2017-12-11 00:00:00', 2, '2019-12-11 00:00:00', '11 de cada mes', '12 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(15, 21, 1, NULL, '2017-12-11 00:00:00', 2, '2019-12-11 00:00:00', '11 de cada mes', '12 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(16, 22, 1, NULL, '2017-12-12 00:00:00', 1, '2018-12-12 00:00:00', '12 de cada mes', '13 de cada mes', '1.00', '300.00', '0.00', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(17, 26, 1, NULL, '2017-12-27 10:20:33', 1, '2018-12-27 10:20:33', '27', '28', '1.00', '300.00', '0.00', 'contado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(18, 2, 1, NULL, '2017-12-27 12:38:07', 1, '2018-12-27 12:38:07', '27', '28', '1.00', '300.00', '0.00', 'contado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(19, 28, 1, NULL, '2017-12-29 00:00:00', 1, '2018-12-29 00:00:00', '29 de cada mes', '30 de cada mes', '1.00', '300.00', '26.48', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(20, 29, 1, NULL, '2017-12-29 00:00:00', 2, '2019-12-29 00:00:00', '29 de cada mes', '30 de cada mes', '1.00', '300.00', '15.40', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(21, 4, 1, NULL, '2017-12-29 09:56:12', 1, '2018-12-29 09:56:12', '29', '30', '1.00', '300.00', '23.08', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(22, 5, 1, NULL, '2017-12-29 10:04:15', 1, '2018-12-29 10:04:15', '29', '30', '1.00', '300.00', '23.08', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(23, 24, 1, NULL, '2017-12-29 10:26:33', 1, '2018-12-29 10:26:33', '29', '30', '1.00', '300.00', '26.50', 'mensual', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(24, 24, 1, NULL, '2017-12-29 11:41:14', 1, '2018-12-29 11:41:14', '29', '30', '1.00', '300.00', '0.00', 'contado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(25, 24, 1, NULL, '2017-12-29 11:49:20', 1, '2018-12-29 11:49:20', '29', '30', '1.00', '300.00', '0.00', 'contado', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `format` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exchange_rate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pago`
--

CREATE TABLE `detalle_pago` (
  `iddetalle_pago` int(11) NOT NULL,
  `idpago_mensualidad` int(11) NOT NULL,
  `idmensualidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_pago`
--

INSERT INTO `detalle_pago` (`iddetalle_pago`, `idpago_mensualidad`, `idmensualidad`) VALUES
(1, 1, 79),
(2, 2, 92),
(3, 3, 105),
(4, 4, 118),
(5, 5, 143),
(6, 6, 168),
(7, 7, 181),
(8, 8, 194),
(9, 9, 219),
(10, 10, 244),
(11, 11, 257),
(12, 12, 270),
(13, 13, 283),
(14, 14, 296),
(15, 15, 321),
(16, 16, 334),
(17, 17, 347),
(18, 18, 360),
(19, 19, 373);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensualidad`
--

CREATE TABLE `mensualidad` (
  `idmensualidad` int(11) NOT NULL,
  `idcontrato` int(11) NOT NULL,
  `num_cuota` int(2) NOT NULL,
  `costo` decimal(7,2) NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `estado` varchar(20) NOT NULL,
  `color_estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mensualidad`
--

INSERT INTO `mensualidad` (`idmensualidad`, `idcontrato`, `num_cuota`, `costo`, `fecha_pago`, `estado`, `color_estado`) VALUES
(1, 1, 1, '847.00', '2017-11-15 00:00:00', 'pendiente', 'amarillo'),
(2, 1, 2, '546.00', '2017-12-15 00:00:00', 'pendiente', 'amarillo'),
(3, 1, 3, '546.00', '2018-01-15 00:00:00', 'pendiente', 'amarillo'),
(4, 1, 4, '546.00', '2018-02-15 00:00:00', 'pendiente', 'amarillo'),
(5, 1, 5, '546.00', '2018-03-15 00:00:00', 'pendiente', 'amarillo'),
(6, 1, 6, '546.00', '2018-04-15 00:00:00', 'pendiente', 'amarillo'),
(7, 1, 7, '546.00', '2018-05-15 00:00:00', 'pendiente', 'amarillo'),
(8, 1, 8, '546.00', '2018-06-15 00:00:00', 'pendiente', 'amarillo'),
(9, 1, 9, '546.00', '2018-07-15 00:00:00', 'pendiente', 'amarillo'),
(10, 1, 10, '546.00', '2018-08-15 00:00:00', 'pendiente', 'amarillo'),
(11, 1, 11, '546.00', '2018-09-15 00:00:00', 'pendiente', 'amarillo'),
(12, 1, 12, '546.00', '2018-10-15 00:00:00', 'pendiente', 'amarillo'),
(13, 1, 13, '546.00', '2018-11-15 00:00:00', 'pendiente', 'amarillo'),
(14, 2, 1, '58.58', '2017-12-05 00:00:00', 'pendiente', 'amarillo'),
(15, 2, 2, '57.58', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(16, 2, 3, '57.58', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(17, 2, 4, '57.58', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(18, 2, 5, '57.58', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(19, 2, 6, '57.58', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(20, 2, 7, '57.58', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(21, 2, 8, '57.58', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(22, 2, 9, '57.58', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(23, 2, 10, '57.58', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(24, 2, 11, '57.58', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(25, 2, 12, '57.58', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(26, 2, 13, '57.58', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(27, 3, 1, '58.58', '2017-12-05 00:00:00', 'pendiente', 'amarillo'),
(28, 3, 2, '57.58', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(29, 3, 3, '57.58', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(30, 3, 4, '57.58', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(31, 3, 5, '57.58', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(32, 3, 6, '57.58', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(33, 3, 7, '57.58', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(34, 3, 8, '57.58', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(35, 3, 9, '57.58', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(36, 3, 10, '57.58', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(37, 3, 11, '57.58', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(38, 3, 12, '57.58', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(39, 3, 13, '57.58', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(40, 4, 1, '58.58', '2017-12-05 00:00:00', 'pendiente', 'amarillo'),
(41, 4, 2, '57.58', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(42, 4, 3, '57.58', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(43, 4, 4, '57.58', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(44, 4, 5, '57.58', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(45, 4, 6, '57.58', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(46, 4, 7, '57.58', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(47, 4, 8, '57.58', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(48, 4, 9, '57.58', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(49, 4, 10, '57.58', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(50, 4, 11, '57.58', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(51, 4, 12, '57.58', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(52, 4, 13, '57.58', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(53, 5, 1, '58.58', '2017-12-05 00:00:00', 'pendiente', 'amarillo'),
(54, 5, 2, '57.58', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(55, 5, 3, '57.58', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(56, 5, 4, '57.58', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(57, 5, 5, '57.58', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(58, 5, 6, '57.58', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(59, 5, 7, '57.58', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(60, 5, 8, '57.58', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(61, 5, 9, '57.58', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(62, 5, 10, '57.58', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(63, 5, 11, '57.58', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(64, 5, 12, '57.58', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(65, 5, 13, '57.58', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(66, 6, 1, '58.58', '2017-12-05 00:00:00', 'pendiente', 'amarillo'),
(67, 6, 2, '57.58', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(68, 6, 3, '57.58', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(69, 6, 4, '57.58', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(70, 6, 5, '57.58', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(71, 6, 6, '57.58', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(72, 6, 7, '57.58', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(73, 6, 8, '57.58', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(74, 6, 9, '57.58', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(75, 6, 10, '57.58', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(76, 6, 11, '57.58', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(77, 6, 12, '57.58', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(78, 6, 13, '57.58', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(79, 7, 1, '58.58', '2017-12-05 00:00:00', 'pagado', 'verde'),
(80, 7, 2, '57.58', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(81, 7, 3, '57.58', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(82, 7, 4, '57.58', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(83, 7, 5, '57.58', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(84, 7, 6, '57.58', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(85, 7, 7, '57.58', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(86, 7, 8, '57.58', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(87, 7, 9, '57.58', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(88, 7, 10, '57.58', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(89, 7, 11, '57.58', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(90, 7, 12, '57.58', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(91, 7, 13, '57.58', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(92, 8, 1, '570.08', '2017-12-05 00:00:00', 'pagado', 'verde'),
(93, 8, 2, '569.08', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(94, 8, 3, '569.08', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(95, 8, 4, '569.08', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(96, 8, 5, '569.08', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(97, 8, 6, '569.08', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(98, 8, 7, '569.08', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(99, 8, 8, '569.08', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(100, 8, 9, '569.08', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(101, 8, 10, '569.08', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(102, 8, 11, '569.08', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(103, 8, 12, '569.08', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(104, 8, 13, '569.08', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(105, 9, 1, '570.08', '2017-12-05 00:00:00', 'pagado', 'verde'),
(106, 9, 2, '569.08', '2018-01-05 00:00:00', 'pendiente', 'amarillo'),
(107, 9, 3, '569.08', '2018-02-05 00:00:00', 'pendiente', 'amarillo'),
(108, 9, 4, '569.08', '2018-03-05 00:00:00', 'pendiente', 'amarillo'),
(109, 9, 5, '569.08', '2018-04-05 00:00:00', 'pendiente', 'amarillo'),
(110, 9, 6, '569.08', '2018-05-05 00:00:00', 'pendiente', 'amarillo'),
(111, 9, 7, '569.08', '2018-06-05 00:00:00', 'pendiente', 'amarillo'),
(112, 9, 8, '569.08', '2018-07-05 00:00:00', 'pendiente', 'amarillo'),
(113, 9, 9, '569.08', '2018-08-05 00:00:00', 'pendiente', 'amarillo'),
(114, 9, 10, '569.08', '2018-09-05 00:00:00', 'pendiente', 'amarillo'),
(115, 9, 11, '569.08', '2018-10-05 00:00:00', 'pendiente', 'amarillo'),
(116, 9, 12, '569.08', '2018-11-05 00:00:00', 'pendiente', 'amarillo'),
(117, 9, 13, '569.08', '2018-12-05 00:00:00', 'pendiente', 'amarillo'),
(118, 10, 1, '558.54', '2017-12-06 00:00:00', 'pagado', 'verde'),
(119, 10, 2, '557.54', '2018-01-06 00:00:00', 'pendiente', 'amarillo'),
(120, 10, 3, '557.54', '2018-02-06 00:00:00', 'pendiente', 'amarillo'),
(121, 10, 4, '557.54', '2018-03-06 00:00:00', 'pendiente', 'amarillo'),
(122, 10, 5, '557.54', '2018-04-06 00:00:00', 'pendiente', 'amarillo'),
(123, 10, 6, '557.54', '2018-05-06 00:00:00', 'pendiente', 'amarillo'),
(124, 10, 7, '557.54', '2018-06-06 00:00:00', 'pendiente', 'amarillo'),
(125, 10, 8, '557.54', '2018-07-06 00:00:00', 'pendiente', 'amarillo'),
(126, 10, 9, '557.54', '2018-08-06 00:00:00', 'pendiente', 'amarillo'),
(127, 10, 10, '557.54', '2018-09-06 00:00:00', 'pendiente', 'amarillo'),
(128, 10, 11, '557.54', '2018-10-06 00:00:00', 'pendiente', 'amarillo'),
(129, 10, 12, '557.54', '2018-11-06 00:00:00', 'pendiente', 'amarillo'),
(130, 10, 13, '557.54', '2018-12-06 00:00:00', 'pendiente', 'amarillo'),
(131, 10, 14, '557.54', '2019-01-06 00:00:00', 'pendiente', 'amarillo'),
(132, 10, 15, '557.54', '2019-02-06 00:00:00', 'pendiente', 'amarillo'),
(133, 10, 16, '557.54', '2019-03-06 00:00:00', 'pendiente', 'amarillo'),
(134, 10, 17, '557.54', '2019-04-06 00:00:00', 'pendiente', 'amarillo'),
(135, 10, 18, '557.54', '2019-05-06 00:00:00', 'pendiente', 'amarillo'),
(136, 10, 19, '557.54', '2019-06-06 00:00:00', 'pendiente', 'amarillo'),
(137, 10, 20, '557.54', '2019-07-06 00:00:00', 'pendiente', 'amarillo'),
(138, 10, 21, '557.54', '2019-08-06 00:00:00', 'pendiente', 'amarillo'),
(139, 10, 22, '557.54', '2019-09-06 00:00:00', 'pendiente', 'amarillo'),
(140, 10, 23, '557.54', '2019-10-06 00:00:00', 'pendiente', 'amarillo'),
(141, 10, 24, '557.54', '2019-11-06 00:00:00', 'pendiente', 'amarillo'),
(142, 10, 25, '557.54', '2019-12-06 00:00:00', 'pendiente', 'amarillo'),
(143, 11, 1, '60.94', '2017-12-10 00:00:00', 'pagado', 'verde'),
(144, 11, 2, '59.94', '2018-01-10 00:00:00', 'pendiente', 'amarillo'),
(145, 11, 3, '59.94', '2018-02-10 00:00:00', 'pendiente', 'amarillo'),
(146, 11, 4, '59.94', '2018-03-10 00:00:00', 'pendiente', 'amarillo'),
(147, 11, 5, '59.94', '2018-04-10 00:00:00', 'pendiente', 'amarillo'),
(148, 11, 6, '59.94', '2018-05-10 00:00:00', 'pendiente', 'amarillo'),
(149, 11, 7, '59.94', '2018-06-10 00:00:00', 'pendiente', 'amarillo'),
(150, 11, 8, '59.94', '2018-07-10 00:00:00', 'pendiente', 'amarillo'),
(151, 11, 9, '59.94', '2018-08-10 00:00:00', 'pendiente', 'amarillo'),
(152, 11, 10, '59.94', '2018-09-10 00:00:00', 'pendiente', 'amarillo'),
(153, 11, 11, '59.94', '2018-10-10 00:00:00', 'pendiente', 'amarillo'),
(154, 11, 12, '59.94', '2018-11-10 00:00:00', 'pendiente', 'amarillo'),
(155, 11, 13, '59.94', '2018-12-10 00:00:00', 'pendiente', 'amarillo'),
(156, 11, 14, '59.94', '2019-01-10 00:00:00', 'pendiente', 'amarillo'),
(157, 11, 15, '59.94', '2019-02-10 00:00:00', 'pendiente', 'amarillo'),
(158, 11, 16, '59.94', '2019-03-10 00:00:00', 'pendiente', 'amarillo'),
(159, 11, 17, '59.94', '2019-04-10 00:00:00', 'pendiente', 'amarillo'),
(160, 11, 18, '59.94', '2019-05-10 00:00:00', 'pendiente', 'amarillo'),
(161, 11, 19, '59.94', '2019-06-10 00:00:00', 'pendiente', 'amarillo'),
(162, 11, 20, '59.94', '2019-07-10 00:00:00', 'pendiente', 'amarillo'),
(163, 11, 21, '59.94', '2019-08-10 00:00:00', 'pendiente', 'amarillo'),
(164, 11, 22, '59.94', '2019-09-10 00:00:00', 'pendiente', 'amarillo'),
(165, 11, 23, '59.94', '2019-10-10 00:00:00', 'pendiente', 'amarillo'),
(166, 11, 24, '59.94', '2019-11-10 00:00:00', 'pendiente', 'amarillo'),
(167, 11, 25, '59.94', '2019-12-10 00:00:00', 'pendiente', 'amarillo'),
(168, 12, 1, '72.48', '2017-12-10 00:00:00', 'anulado', 'rojo'),
(169, 12, 2, '71.48', '2018-01-10 00:00:00', 'anulado', 'rojo'),
(170, 12, 3, '71.48', '2018-02-10 00:00:00', 'anulado', 'rojo'),
(171, 12, 4, '71.48', '2018-03-10 00:00:00', 'anulado', 'rojo'),
(172, 12, 5, '71.48', '2018-04-10 00:00:00', 'anulado', 'rojo'),
(173, 12, 6, '71.48', '2018-05-10 00:00:00', 'anulado', 'rojo'),
(174, 12, 7, '71.48', '2018-06-10 00:00:00', 'anulado', 'rojo'),
(175, 12, 8, '71.48', '2018-07-10 00:00:00', 'anulado', 'rojo'),
(176, 12, 9, '71.48', '2018-08-10 00:00:00', 'anulado', 'rojo'),
(177, 12, 10, '71.48', '2018-09-10 00:00:00', 'anulado', 'rojo'),
(178, 12, 11, '71.48', '2018-10-10 00:00:00', 'anulado', 'rojo'),
(179, 12, 12, '71.48', '2018-11-10 00:00:00', 'anulado', 'rojo'),
(180, 12, 13, '71.48', '2018-12-10 00:00:00', 'anulado', 'rojo'),
(181, 13, 1, '72.48', '2017-12-11 00:00:00', 'pagado', 'verde'),
(182, 13, 2, '71.48', '2018-01-11 00:00:00', 'pendiente', 'amarillo'),
(183, 13, 3, '71.48', '2018-02-11 00:00:00', 'pendiente', 'amarillo'),
(184, 13, 4, '71.48', '2018-03-11 00:00:00', 'pendiente', 'amarillo'),
(185, 13, 5, '71.48', '2018-04-11 00:00:00', 'pendiente', 'amarillo'),
(186, 13, 6, '71.48', '2018-05-11 00:00:00', 'pendiente', 'amarillo'),
(187, 13, 7, '71.48', '2018-06-11 00:00:00', 'pendiente', 'amarillo'),
(188, 13, 8, '71.48', '2018-07-11 00:00:00', 'pendiente', 'amarillo'),
(189, 13, 9, '71.48', '2018-08-11 00:00:00', 'pendiente', 'amarillo'),
(190, 13, 10, '71.48', '2018-09-11 00:00:00', 'pendiente', 'amarillo'),
(191, 13, 11, '71.48', '2018-10-11 00:00:00', 'pendiente', 'amarillo'),
(192, 13, 12, '71.48', '2018-11-11 00:00:00', 'pendiente', 'amarillo'),
(193, 13, 13, '71.48', '2018-12-11 00:00:00', 'pendiente', 'amarillo'),
(194, 14, 1, '65.84', '2017-12-11 00:00:00', 'pagado', 'verde'),
(195, 14, 2, '64.84', '2018-01-11 00:00:00', 'pendiente', 'amarillo'),
(196, 14, 3, '64.84', '2018-02-11 00:00:00', 'pendiente', 'amarillo'),
(197, 14, 4, '64.84', '2018-03-11 00:00:00', 'pendiente', 'amarillo'),
(198, 14, 5, '64.84', '2018-04-11 00:00:00', 'pendiente', 'amarillo'),
(199, 14, 6, '64.84', '2018-05-11 00:00:00', 'pendiente', 'amarillo'),
(200, 14, 7, '64.84', '2018-06-11 00:00:00', 'pendiente', 'amarillo'),
(201, 14, 8, '64.84', '2018-07-11 00:00:00', 'pendiente', 'amarillo'),
(202, 14, 9, '64.84', '2018-08-11 00:00:00', 'pendiente', 'amarillo'),
(203, 14, 10, '64.84', '2018-09-11 00:00:00', 'pendiente', 'amarillo'),
(204, 14, 11, '64.84', '2018-10-11 00:00:00', 'pendiente', 'amarillo'),
(205, 14, 12, '64.84', '2018-11-11 00:00:00', 'pendiente', 'amarillo'),
(206, 14, 13, '64.84', '2018-12-11 00:00:00', 'pendiente', 'amarillo'),
(207, 14, 14, '64.84', '2019-01-11 00:00:00', 'pendiente', 'amarillo'),
(208, 14, 15, '64.84', '2019-02-11 00:00:00', 'pendiente', 'amarillo'),
(209, 14, 16, '64.84', '2019-03-11 00:00:00', 'pendiente', 'amarillo'),
(210, 14, 17, '64.84', '2019-04-11 00:00:00', 'pendiente', 'amarillo'),
(211, 14, 18, '64.84', '2019-05-11 00:00:00', 'pendiente', 'amarillo'),
(212, 14, 19, '64.84', '2019-06-11 00:00:00', 'pendiente', 'amarillo'),
(213, 14, 20, '64.84', '2019-07-11 00:00:00', 'pendiente', 'amarillo'),
(214, 14, 21, '64.84', '2019-08-11 00:00:00', 'pendiente', 'amarillo'),
(215, 14, 22, '64.84', '2019-09-11 00:00:00', 'pendiente', 'amarillo'),
(216, 14, 23, '64.84', '2019-10-11 00:00:00', 'pendiente', 'amarillo'),
(217, 14, 24, '64.84', '2019-11-11 00:00:00', 'pendiente', 'amarillo'),
(218, 14, 25, '64.84', '2019-12-11 00:00:00', 'pendiente', 'amarillo'),
(219, 15, 1, '65.84', '2017-12-11 00:00:00', 'pagado', 'verde'),
(220, 15, 2, '64.84', '2018-01-11 00:00:00', 'pendiente', 'amarillo'),
(221, 15, 3, '64.84', '2018-02-11 00:00:00', 'pendiente', 'amarillo'),
(222, 15, 4, '64.84', '2018-03-11 00:00:00', 'pendiente', 'amarillo'),
(223, 15, 5, '64.84', '2018-04-11 00:00:00', 'pendiente', 'amarillo'),
(224, 15, 6, '64.84', '2018-05-11 00:00:00', 'pendiente', 'amarillo'),
(225, 15, 7, '64.84', '2018-06-11 00:00:00', 'pendiente', 'amarillo'),
(226, 15, 8, '64.84', '2018-07-11 00:00:00', 'pendiente', 'amarillo'),
(227, 15, 9, '64.84', '2018-08-11 00:00:00', 'pendiente', 'amarillo'),
(228, 15, 10, '64.84', '2018-09-11 00:00:00', 'pendiente', 'amarillo'),
(229, 15, 11, '64.84', '2018-10-11 00:00:00', 'pendiente', 'amarillo'),
(230, 15, 12, '64.84', '2018-11-11 00:00:00', 'pendiente', 'amarillo'),
(231, 15, 13, '64.84', '2018-12-11 00:00:00', 'pendiente', 'amarillo'),
(232, 15, 14, '64.84', '2019-01-11 00:00:00', 'pendiente', 'amarillo'),
(233, 15, 15, '64.84', '2019-02-11 00:00:00', 'pendiente', 'amarillo'),
(234, 15, 16, '64.84', '2019-03-11 00:00:00', 'pendiente', 'amarillo'),
(235, 15, 17, '64.84', '2019-04-11 00:00:00', 'pendiente', 'amarillo'),
(236, 15, 18, '64.84', '2019-05-11 00:00:00', 'pendiente', 'amarillo'),
(237, 15, 19, '64.84', '2019-06-11 00:00:00', 'pendiente', 'amarillo'),
(238, 15, 20, '64.84', '2019-07-11 00:00:00', 'pendiente', 'amarillo'),
(239, 15, 21, '64.84', '2019-08-11 00:00:00', 'pendiente', 'amarillo'),
(240, 15, 22, '64.84', '2019-09-11 00:00:00', 'pendiente', 'amarillo'),
(241, 15, 23, '64.84', '2019-10-11 00:00:00', 'pendiente', 'amarillo'),
(242, 15, 24, '64.84', '2019-11-11 00:00:00', 'pendiente', 'amarillo'),
(243, 15, 25, '64.84', '2019-12-11 00:00:00', 'pendiente', 'amarillo'),
(244, 16, 1, '77.38', '2017-12-12 00:00:00', 'pagado', 'verde'),
(245, 16, 2, '76.38', '2018-01-12 00:00:00', 'pendiente', 'amarillo'),
(246, 16, 3, '76.38', '2018-02-12 00:00:00', 'pendiente', 'amarillo'),
(247, 16, 4, '76.38', '2018-03-12 00:00:00', 'pendiente', 'amarillo'),
(248, 16, 5, '76.38', '2018-04-12 00:00:00', 'pendiente', 'amarillo'),
(249, 16, 6, '76.38', '2018-05-12 00:00:00', 'pendiente', 'amarillo'),
(250, 16, 7, '76.38', '2018-06-12 00:00:00', 'pendiente', 'amarillo'),
(251, 16, 8, '76.38', '2018-07-12 00:00:00', 'pendiente', 'amarillo'),
(252, 16, 9, '76.38', '2018-08-12 00:00:00', 'pendiente', 'amarillo'),
(253, 16, 10, '76.38', '2018-09-12 00:00:00', 'pendiente', 'amarillo'),
(254, 16, 11, '76.38', '2018-10-12 00:00:00', 'pendiente', 'amarillo'),
(255, 16, 12, '76.38', '2018-11-12 00:00:00', 'pendiente', 'amarillo'),
(256, 16, 13, '76.38', '2018-12-12 00:00:00', 'pendiente', 'amarillo'),
(257, 17, 1, '350.90', '2017-12-27 00:00:00', 'pagado', 'verde'),
(258, 17, 2, '49.90', '2018-01-27 00:00:00', 'pendiente', 'amarillo'),
(259, 17, 3, '49.90', '2018-02-27 00:00:00', 'pendiente', 'amarillo'),
(260, 17, 4, '49.90', '2018-03-27 00:00:00', 'pendiente', 'amarillo'),
(261, 17, 5, '49.90', '2018-04-27 00:00:00', 'pendiente', 'amarillo'),
(262, 17, 6, '49.90', '2018-05-27 00:00:00', 'pendiente', 'amarillo'),
(263, 17, 7, '49.90', '2018-06-27 00:00:00', 'pendiente', 'amarillo'),
(264, 17, 8, '49.90', '2018-07-27 00:00:00', 'pendiente', 'amarillo'),
(265, 17, 9, '49.90', '2018-08-27 00:00:00', 'pendiente', 'amarillo'),
(266, 17, 10, '49.90', '2018-09-27 00:00:00', 'pendiente', 'amarillo'),
(267, 17, 11, '49.90', '2018-10-27 00:00:00', 'pendiente', 'amarillo'),
(268, 17, 12, '49.90', '2018-11-27 00:00:00', 'pendiente', 'amarillo'),
(269, 17, 13, '49.90', '2018-12-27 00:00:00', 'pendiente', 'amarillo'),
(270, 18, 1, '350.90', '2017-12-27 00:00:00', 'pagado', 'verde'),
(271, 18, 2, '49.90', '2018-01-27 00:00:00', 'pendiente', 'amarillo'),
(272, 18, 3, '49.90', '2018-02-27 00:00:00', 'pendiente', 'amarillo'),
(273, 18, 4, '49.90', '2018-03-27 00:00:00', 'pendiente', 'amarillo'),
(274, 18, 5, '49.90', '2018-04-27 00:00:00', 'pendiente', 'amarillo'),
(275, 18, 6, '49.90', '2018-05-27 00:00:00', 'pendiente', 'amarillo'),
(276, 18, 7, '49.90', '2018-06-27 00:00:00', 'pendiente', 'amarillo'),
(277, 18, 8, '49.90', '2018-07-27 00:00:00', 'pendiente', 'amarillo'),
(278, 18, 9, '49.90', '2018-08-27 00:00:00', 'pendiente', 'amarillo'),
(279, 18, 10, '49.90', '2018-09-27 00:00:00', 'pendiente', 'amarillo'),
(280, 18, 11, '49.90', '2018-10-27 00:00:00', 'pendiente', 'amarillo'),
(281, 18, 12, '49.90', '2018-11-27 00:00:00', 'pendiente', 'amarillo'),
(282, 18, 13, '49.90', '2018-12-27 00:00:00', 'pendiente', 'amarillo'),
(283, 19, 1, '77.38', '2017-12-29 00:00:00', 'pagado', 'verde'),
(284, 19, 2, '76.38', '2018-01-29 00:00:00', 'pendiente', 'amarillo'),
(285, 19, 3, '76.38', '2018-02-28 00:00:00', 'pendiente', 'amarillo'),
(286, 19, 4, '76.38', '2018-03-28 00:00:00', 'pendiente', 'amarillo'),
(287, 19, 5, '76.38', '2018-04-28 00:00:00', 'pendiente', 'amarillo'),
(288, 19, 6, '76.38', '2018-05-28 00:00:00', 'pendiente', 'amarillo'),
(289, 19, 7, '76.38', '2018-06-28 00:00:00', 'pendiente', 'amarillo'),
(290, 19, 8, '76.38', '2018-07-28 00:00:00', 'pendiente', 'amarillo'),
(291, 19, 9, '76.38', '2018-08-28 00:00:00', 'pendiente', 'amarillo'),
(292, 19, 10, '76.38', '2018-09-28 00:00:00', 'pendiente', 'amarillo'),
(293, 19, 11, '76.38', '2018-10-28 00:00:00', 'pendiente', 'amarillo'),
(294, 19, 12, '76.38', '2018-11-28 00:00:00', 'pendiente', 'amarillo'),
(295, 19, 13, '76.38', '2018-12-28 00:00:00', 'pendiente', 'amarillo'),
(296, 20, 1, '66.30', '2017-12-29 00:00:00', 'pagado', 'verde'),
(297, 20, 2, '65.30', '2018-01-29 00:00:00', 'pendiente', 'amarillo'),
(298, 20, 3, '65.30', '2018-02-28 00:00:00', 'pendiente', 'amarillo'),
(299, 20, 4, '65.30', '2018-03-28 00:00:00', 'pendiente', 'amarillo'),
(300, 20, 5, '65.30', '2018-04-28 00:00:00', 'pendiente', 'amarillo'),
(301, 20, 6, '65.30', '2018-05-28 00:00:00', 'pendiente', 'amarillo'),
(302, 20, 7, '65.30', '2018-06-28 00:00:00', 'pendiente', 'amarillo'),
(303, 20, 8, '65.30', '2018-07-28 00:00:00', 'pendiente', 'amarillo'),
(304, 20, 9, '65.30', '2018-08-28 00:00:00', 'pendiente', 'amarillo'),
(305, 20, 10, '65.30', '2018-09-28 00:00:00', 'pendiente', 'amarillo'),
(306, 20, 11, '65.30', '2018-10-28 00:00:00', 'pendiente', 'amarillo'),
(307, 20, 12, '65.30', '2018-11-28 00:00:00', 'pendiente', 'amarillo'),
(308, 20, 13, '65.30', '2018-12-28 00:00:00', 'pendiente', 'amarillo'),
(309, 20, 14, '65.30', '2019-01-28 00:00:00', 'pendiente', 'amarillo'),
(310, 20, 15, '65.30', '2019-02-28 00:00:00', 'pendiente', 'amarillo'),
(311, 20, 16, '65.30', '2019-03-28 00:00:00', 'pendiente', 'amarillo'),
(312, 20, 17, '65.30', '2019-04-28 00:00:00', 'pendiente', 'amarillo'),
(313, 20, 18, '65.30', '2019-05-28 00:00:00', 'pendiente', 'amarillo'),
(314, 20, 19, '65.30', '2019-06-28 00:00:00', 'pendiente', 'amarillo'),
(315, 20, 20, '65.30', '2019-07-28 00:00:00', 'pendiente', 'amarillo'),
(316, 20, 21, '65.30', '2019-08-28 00:00:00', 'pendiente', 'amarillo'),
(317, 20, 22, '65.30', '2019-09-28 00:00:00', 'pendiente', 'amarillo'),
(318, 20, 23, '65.30', '2019-10-28 00:00:00', 'pendiente', 'amarillo'),
(319, 20, 24, '65.30', '2019-11-28 00:00:00', 'pendiente', 'amarillo'),
(320, 20, 25, '65.30', '2019-12-28 00:00:00', 'pendiente', 'amarillo'),
(321, 21, 1, '73.98', '2017-12-29 00:00:00', 'pagado', 'verde'),
(322, 21, 2, '72.98', '2018-01-29 00:00:00', 'pendiente', 'amarillo'),
(323, 21, 3, '72.98', '2018-02-28 00:00:00', 'pendiente', 'amarillo'),
(324, 21, 4, '72.98', '2018-03-28 00:00:00', 'pendiente', 'amarillo'),
(325, 21, 5, '72.98', '2018-04-28 00:00:00', 'pendiente', 'amarillo'),
(326, 21, 6, '72.98', '2018-05-28 00:00:00', 'pendiente', 'amarillo'),
(327, 21, 7, '72.98', '2018-06-28 00:00:00', 'pendiente', 'amarillo'),
(328, 21, 8, '72.98', '2018-07-28 00:00:00', 'pendiente', 'amarillo'),
(329, 21, 9, '72.98', '2018-08-28 00:00:00', 'pendiente', 'amarillo'),
(330, 21, 10, '72.98', '2018-09-28 00:00:00', 'pendiente', 'amarillo'),
(331, 21, 11, '72.98', '2018-10-28 00:00:00', 'pendiente', 'amarillo'),
(332, 21, 12, '72.98', '2018-11-28 00:00:00', 'pendiente', 'amarillo'),
(333, 21, 13, '72.98', '2018-12-28 00:00:00', 'pendiente', 'amarillo'),
(334, 22, 1, '73.98', '2017-12-29 00:00:00', 'pagado', 'verde'),
(335, 22, 2, '72.98', '2018-01-29 00:00:00', 'pendiente', 'amarillo'),
(336, 22, 3, '72.98', '2018-02-28 00:00:00', 'pendiente', 'amarillo'),
(337, 22, 4, '72.98', '2018-03-28 00:00:00', 'pendiente', 'amarillo'),
(338, 22, 5, '72.98', '2018-04-28 00:00:00', 'pendiente', 'amarillo'),
(339, 22, 6, '72.98', '2018-05-28 00:00:00', 'pendiente', 'amarillo'),
(340, 22, 7, '72.98', '2018-06-28 00:00:00', 'pendiente', 'amarillo'),
(341, 22, 8, '72.98', '2018-07-28 00:00:00', 'pendiente', 'amarillo'),
(342, 22, 9, '72.98', '2018-08-28 00:00:00', 'pendiente', 'amarillo'),
(343, 22, 10, '72.98', '2018-09-28 00:00:00', 'pendiente', 'amarillo'),
(344, 22, 11, '72.98', '2018-10-28 00:00:00', 'pendiente', 'amarillo'),
(345, 22, 12, '72.98', '2018-11-28 00:00:00', 'pendiente', 'amarillo'),
(346, 22, 13, '72.98', '2018-12-28 00:00:00', 'pendiente', 'amarillo'),
(347, 23, 1, '77.40', '2017-12-29 00:00:00', 'anulado', 'rojo'),
(348, 23, 2, '76.40', '2018-01-29 00:00:00', 'anulado', 'rojo'),
(349, 23, 3, '76.40', '2018-02-28 00:00:00', 'anulado', 'rojo'),
(350, 23, 4, '76.40', '2018-03-28 00:00:00', 'anulado', 'rojo'),
(351, 23, 5, '76.40', '2018-04-28 00:00:00', 'anulado', 'rojo'),
(352, 23, 6, '76.40', '2018-05-28 00:00:00', 'anulado', 'rojo'),
(353, 23, 7, '76.40', '2018-06-28 00:00:00', 'anulado', 'rojo'),
(354, 23, 8, '76.40', '2018-07-28 00:00:00', 'anulado', 'rojo'),
(355, 23, 9, '76.40', '2018-08-28 00:00:00', 'anulado', 'rojo'),
(356, 23, 10, '76.40', '2018-09-28 00:00:00', 'anulado', 'rojo'),
(357, 23, 11, '76.40', '2018-10-28 00:00:00', 'anulado', 'rojo'),
(358, 23, 12, '76.40', '2018-11-28 00:00:00', 'anulado', 'rojo'),
(359, 23, 13, '76.40', '2018-12-28 00:00:00', 'anulado', 'rojo'),
(360, 24, 1, '350.90', '2017-12-29 00:00:00', 'pagado', 'verde'),
(361, 24, 2, '49.90', '2018-01-29 00:00:00', 'pendiente', 'amarillo'),
(362, 24, 3, '49.90', '2018-02-28 00:00:00', 'pendiente', 'amarillo'),
(363, 24, 4, '49.90', '2018-03-28 00:00:00', 'pendiente', 'amarillo'),
(364, 24, 5, '49.90', '2018-04-28 00:00:00', 'pendiente', 'amarillo'),
(365, 24, 6, '49.90', '2018-05-28 00:00:00', 'pendiente', 'amarillo'),
(366, 24, 7, '49.90', '2018-06-28 00:00:00', 'pendiente', 'amarillo'),
(367, 24, 8, '49.90', '2018-07-28 00:00:00', 'pendiente', 'amarillo'),
(368, 24, 9, '49.90', '2018-08-28 00:00:00', 'pendiente', 'amarillo'),
(369, 24, 10, '49.90', '2018-09-28 00:00:00', 'pendiente', 'amarillo'),
(370, 24, 11, '49.90', '2018-10-28 00:00:00', 'pendiente', 'amarillo'),
(371, 24, 12, '49.90', '2018-11-28 00:00:00', 'pendiente', 'amarillo'),
(372, 24, 13, '49.90', '2018-12-28 00:00:00', 'pendiente', 'amarillo'),
(373, 25, 1, '350.90', '2017-12-29 00:00:00', 'anulado', 'rojo'),
(374, 25, 2, '49.90', '2018-01-29 00:00:00', 'anulado', 'rojo'),
(375, 25, 3, '49.90', '2018-02-28 00:00:00', 'anulado', 'rojo'),
(376, 25, 4, '49.90', '2018-03-28 00:00:00', 'anulado', 'rojo'),
(377, 25, 5, '49.90', '2018-04-28 00:00:00', 'anulado', 'rojo'),
(378, 25, 6, '49.90', '2018-05-28 00:00:00', 'anulado', 'rojo'),
(379, 25, 7, '49.90', '2018-06-28 00:00:00', 'anulado', 'rojo'),
(380, 25, 8, '49.90', '2018-07-28 00:00:00', 'anulado', 'rojo'),
(381, 25, 9, '49.90', '2018-08-28 00:00:00', 'anulado', 'rojo'),
(382, 25, 10, '49.90', '2018-09-28 00:00:00', 'anulado', 'rojo'),
(383, 25, 11, '49.90', '2018-10-28 00:00:00', 'anulado', 'rojo'),
(384, 25, 12, '49.90', '2018-11-28 00:00:00', 'anulado', 'rojo'),
(385, 25, 13, '49.90', '2018-12-28 00:00:00', 'anulado', 'rojo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_11_26_161501_create_currency_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_mensualidad`
--

CREATE TABLE `pago_mensualidad` (
  `idpago_mensualidad` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `fecha_hora_pagado` datetime NOT NULL,
  `tipo_comprobante` varchar(20) NOT NULL,
  `serie_comprobante` varchar(10) DEFAULT NULL,
  `num_comprobante` varchar(15) NOT NULL,
  `mora` decimal(5,2) DEFAULT '0.00',
  `total_pago` decimal(5,2) NOT NULL,
  `costo_mensualidad` decimal(5,2) NOT NULL,
  `estado` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pago_mensualidad`
--

INSERT INTO `pago_mensualidad` (`idpago_mensualidad`, `idcliente`, `fecha_hora_pagado`, `tipo_comprobante`, `serie_comprobante`, `num_comprobante`, `mora`, `total_pago`, `costo_mensualidad`, `estado`) VALUES
(1, 13, '2017-12-05 00:00:00', 'boleta', 'serie', 'bnabr4zrdx', '0.00', '59.00', '0.00', ''),
(2, 14, '2017-12-05 00:00:00', 'boleta', '', 'cj7pktpqbs', '0.00', '570.00', '570.08', 'pagado'),
(3, 15, '2017-12-05 00:00:00', 'boleta', '', 'n02erlr5za', '0.00', '570.00', '570.08', 'pagado'),
(4, 16, '2017-12-06 00:00:00', 'boleta', '', 'rt7hmratvu', '0.00', '558.54', '558.54', 'pagado'),
(5, 17, '2017-12-10 00:00:00', 'boleta', '', '5mcjd3slzh', '0.00', '60.94', '60.94', 'pagado'),
(6, 18, '2017-12-10 00:00:00', 'factura', '', 'lbipvxeuy4', '0.00', '72.48', '72.48', 'pagado'),
(7, 19, '2017-12-11 09:43:57', 'factura', '', 'i39f9emlrv', '0.00', '72.48', '72.48', 'pagado'),
(8, 20, '2017-12-11 13:03:58', 'boleta', '', 'lhewy4jav6', '0.00', '65.84', '65.84', 'pagado'),
(9, 21, '2017-12-11 13:11:30', 'boleta', '', '2lz6m4diuc', '0.00', '65.84', '65.84', 'pagado'),
(10, 22, '2017-12-12 11:09:12', 'boleta', '', '1k4r9bii0t', '0.00', '77.38', '77.38', 'pagado'),
(11, 26, '2017-12-27 10:20:33', 'boleta', '1245457', '21212454', '0.00', '350.90', '350.90', 'pagado'),
(12, 2, '2017-12-27 12:38:07', 'boleta', NULL, '123456', '0.00', '350.90', '350.90', 'pagado'),
(13, 28, '2017-12-29 09:41:25', 'boleta', '', 'gyt28wj1dt', '0.00', '77.38', '77.38', 'pagado'),
(14, 29, '2017-12-29 09:43:56', 'boleta', '', '9jxud2z66n', '0.00', '66.30', '66.30', 'pagado'),
(15, 4, '2017-12-29 09:56:12', 'boleta', NULL, '123456', '0.00', '73.98', '73.98', 'pagado'),
(16, 5, '2017-12-29 10:04:15', 'boleta', NULL, 'ZDAS', '0.00', '73.98', '73.98', 'pagado'),
(17, 24, '2017-12-29 10:26:33', 'boleta', NULL, '21212454', '0.00', '77.40', '77.40', 'pagado'),
(18, 24, '2017-12-29 11:41:14', 'boleta', NULL, '21212454', '0.00', '350.90', '350.90', 'pagado'),
(19, 24, '2017-12-29 11:49:20', 'boleta', NULL, '21212454', '0.00', '350.90', '350.90', 'pagado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquete`
--

CREATE TABLE `paquete` (
  `idpaquete` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `megas_subida` decimal(5,2) DEFAULT NULL,
  `megas_bajada` decimal(5,2) DEFAULT NULL,
  `megas_subida_comercial` decimal(5,2) NOT NULL,
  `megas_descarga_comercial` decimal(5,2) NOT NULL,
  `precio_mensual` decimal(5,2) NOT NULL,
  `observacion` varchar(80) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paquete`
--

INSERT INTO `paquete` (`idpaquete`, `nombre`, `megas_subida`, `megas_bajada`, `megas_subida_comercial`, `megas_descarga_comercial`, `precio_mensual`, `observacion`, `estado`) VALUES
(1, 'economico', '0.25', '0.75', '0.50', '1.00', '49.90', NULL, 1),
(2, 'standar', '0.50', '1.50', '1.00', '2.00', '79.90', '6546', 1),
(3, 'premium', '1.50', '3.00', '2.00', '4.00', '139.90', NULL, 1),
(6, 'e', '324.00', '34.00', '44.00', '44.00', '554.00', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicidad`
--

CREATE TABLE `publicidad` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `tipo_imagen` varchar(8) NOT NULL DEFAULT 'peque',
  `ruta_imagen` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publicidad`
--

INSERT INTO `publicidad` (`id`, `titulo`, `tipo_imagen`, `ruta_imagen`) VALUES
(51, 'vive conectado ilimitadamente con onenet', 'peque01', 'img/paginaweb/publi/publicidad0151.png'),
(52, 'intenet inalambrico onenet', 'peque02', 'img/paginaweb/publi/publicidad0252.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `idcliente` int(11) DEFAULT NULL,
  `tipo_usuario` tinyint(4) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `idcliente`, `tipo_usuario`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `estado`) VALUES
(1, NULL, 1, 'admin', 'admin@gmail.com', '$2y$10$6Bft7kSvU0iJphiqxqNq9uFTES2gSR/6H04ZumwhzqbZiVV/k9sSW', 'VzuJ7wCkrpsChntDR0G2r8L0anwkrD62QtqdLGyoqXcBUnhiUmgCp6a1zRqC', '2017-11-15 17:09:01', '2017-11-19 14:55:32', 1),
(2, 1, 0, '123456', 'cristian@gmail.com', '$2y$10$gZlsa.evpsW48sPzK8wOhuHZzEE49qTQT1a4CxaUuaJyfmucL/Lc2', 'iQuJOKUTHpUz695C61dme6Ju48GgBBDn9ykH8wFbnNwICb6K4uuNue7jhxA2', '2017-11-15 17:26:00', '2017-11-15 17:26:00', 1),
(3, 2, 0, '3244', 'admin@gmail.com', '123456', NULL, '2017-11-15 17:32:27', '2017-11-19 14:51:28', 1),
(4, 3, 0, '324', 'admin@gmail.com', '$2y$10$XApMa3W2DF533IhnknF52OhVmqzEpQ6.Ap2yxAQC/gPB1U.AC2qu2', NULL, '2017-11-18 13:21:28', '2017-11-18 13:21:28', 1),
(6, 4, 0, '3245', 'cristian@gmail.com', '$2y$10$xceIaeDmubSIuxNKYlta4OnKJmB2GBrbDwXiU577wKDxgkU0R.R/S', NULL, '2017-11-19 14:53:06', '2017-11-19 14:53:06', 1),
(7, 5, 0, '234', 'cristian@gmail.com', '$2y$10$XdcJ76quOHzT9QB0br7j1.YHrKiLaezmoN8uJAX59r.YBovzctx6i', 'brkZYlWOZZMB9t8nHFIYu4Hx4CQ5ZJUIE9l0tpqQQ3i9DeYKWdkdETAlWEbu', '2017-12-05 14:43:35', '2017-12-05 14:43:35', 1),
(8, 6, 0, '234', 'cristian@gmail.com', '$2y$10$8K1B80tUQBuueuDfgkzmDOrSsteOPk6tTq0oCq8p6NE2rcmA1NIye', NULL, '2017-12-05 23:14:24', '2017-12-05 23:14:24', 1),
(9, 7, 0, '234', 'cristian@gmail.com', '$2y$10$m/mpPhON5a.319fWTI2gA.TGDvd/mSyQxNULC.02IKPzFLbR9JO.m', NULL, '2017-12-05 23:15:52', '2017-12-05 23:15:52', 1),
(10, 8, 0, '234', 'cristian@gmail.com', '$2y$10$1l9QQrPO8Ib4Id9pQzax2OXEjfQg/Rmnu3l/Dk15EZZ2oPWYtJtdC', NULL, '2017-12-05 23:18:26', '2017-12-05 23:18:26', 1),
(11, 9, 0, '234', 'cristian@gmail.com', '$2y$10$4uTYo52bKqky9capnXN9K.SIjnExTbndSB4ZBUU3HfpxMyvR7twJC', NULL, '2017-12-05 23:20:35', '2017-12-05 23:20:35', 1),
(12, 10, 0, '234', 'cristian@gmail.com', '$2y$10$aHTqmTEqjzfEPIZl5ojmo.nVqw7TfKa2rORTJzd99m6ScHyWuD/Sy', NULL, '2017-12-05 23:21:03', '2017-12-05 23:21:03', 1),
(13, 11, 0, '234', 'cristian@gmail.com', '$2y$10$tksQ0ooW/q1ov.G67weY/euYWB3OYNIM5aDFiLxLMaa5/xThgi6u.', NULL, '2017-12-05 23:26:34', '2017-12-05 23:26:34', 1),
(14, 12, 0, '234', 'cristian@gmail.com', '$2y$10$vydAhEnJW3/eELuBBxlkn.sAQXmzCJF2kBEHw8hsACPza5Jfhluc6', NULL, '2017-12-05 23:27:23', '2017-12-05 23:27:23', 1),
(15, 13, 0, '234', 'cristian@gmail.com', '$2y$10$XVZTP/kCbTQmTY5SxL46KuF6jBwoBMFJMxBI7ZAINt2SGS19XTuIy', NULL, '2017-12-05 23:28:17', '2017-12-05 23:28:17', 1),
(16, 14, 0, '234', 'cristian@gmail.com', '$2y$10$6cChZ6pni7qojXuy0WgkJOZijOcXiy6yv78wshLTFvkiRJ.7ACDRa', NULL, '2017-12-05 23:47:38', '2017-12-05 23:47:38', 1),
(17, 15, 0, '737663084', 'cristiancruzbenel@gmail.com', '$2y$10$NoQD5hepj/CrVK/4aDbM0.IW2MOYykOawDDKNhPEa0O/wE5/EK1Iq', 'WnMkqwRtdbNtPrBr31MEg2JgnlWKuwDxdcMrr8I1ypugWqDThRCDA7KGKkta', '2017-12-05 23:57:34', '2017-12-05 23:57:34', 1),
(18, 16, 0, '234', 'cristian@gmail.com', '$2y$10$/15PVovDUt/zeXQc5rWmu.9nEjUVDReUm340x5HUDvvWi43shoNv2', NULL, '2017-12-06 09:25:43', '2017-12-06 09:25:43', 1),
(20, 18, 0, '73766308', 'cristiancruzbenel@gmail.com', '$2y$10$7lNFJiuGAE.vWRItfIUq2udv/KS7LyeFIMQCOoiNj1RqqoIUSLYkW', 'AJQY77uI0SP0OZWpwEWtTKkbbuGv00dYB4TLHjDT7S34XAQoqwsXO3de63Cj', '2017-12-10 15:07:19', '2017-12-10 15:07:19', 1),
(21, 19, 0, '737663087', 'cristiancruzbenel@gmail.com', '$2y$10$QwwqLKnT02encpc.O2ZzbOBN76r0Wc/rF6BYly3/XRxXFtK76vtg6', NULL, '2017-12-11 09:43:57', '2017-12-11 09:43:57', 1),
(22, 20, 0, '737663088', 'cristian@gmail.com', '$2y$10$k9eNVZdlVkeolfIcYQpzquqVVNoz/w956bIBmRF5TSlqAmn7nYfVe', NULL, '2017-12-11 13:03:57', '2017-12-11 13:03:57', 1),
(23, 21, 0, '737663088', 'cristian@gmail.com', '$2y$10$OgQItDxwwHwsTkNTcayA4.ANUUHXZ2JEfGhIlDdiZGc5RGHFNc0Z6', NULL, '2017-12-11 13:11:28', '2017-12-11 13:11:28', 1),
(24, 22, 0, '16571214', 'teoni@gmail.com', '$2y$10$0Xq9tRhkKAOXNoqvmxiISemycQSYISzo3eeASnTR.tuHPiD6vy77C', NULL, '2017-12-12 11:09:11', '2017-12-12 11:09:11', 1),
(26, 24, 0, '2343', 'cristian@gmail.com', '$2y$10$vVaQLZAeQXPFj6VFTFqqM.uFvWFphDNW2z7pX.A5sgYlii8vv6u4O', NULL, '2017-12-26 11:44:42', '2017-12-26 11:44:42', 1),
(27, 25, 0, '2342', 'cristian@gmail.com', '$2y$10$UVoQoSZntmloCoUvwzOj3eDd12/hzXR2d0MVdmOIzDikHFl1DAEFG', NULL, '2017-12-26 23:59:08', '2017-12-26 23:59:08', 1),
(28, 26, 0, '8888', 'marialalcira@hotmail.com', '$2y$10$wOBP7dIFBrVF2SUyvZSkMeYWMGjcuOdXhKNHSmUdRLN6eB8Bo4P/6', NULL, '2017-12-27 10:26:02', '2017-12-27 10:26:02', 1),
(29, 27, 0, '345', 'cristiancruzbenel@gmail.com', '$2y$10$.ChSzfUHloMgTPDFAOzhVutP7eBZMZs7dfFlFiwVEf8OthZO3f9m6', NULL, '2017-12-29 09:40:04', '2017-12-29 09:40:04', 1),
(30, 28, 0, '345', 'cristiancruzbenel@gmail.com', '$2y$10$NpuboC56qN/U2LkGcdom..6XTCPto/QNuB9wKd1dJJ5LnbfAkt6SO', NULL, '2017-12-29 09:41:25', '2017-12-29 09:41:25', 1),
(31, 29, 0, '2322', 'admin@gmail.com', '$2y$10$N7uzsEnEVG7LOPCxQbbYTug2t9uFohIn70Iocbqaj0vphTql99RbS', NULL, '2017-12-29 09:43:55', '2017-12-29 09:43:55', 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_cobros_pendientes`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_cobros_pendientes` (
`idcontrato` int(11)
,`contrato_estado` tinyint(1)
,`idmensualidad` int(11)
,`idcliente` int(11)
,`nombre_razon` varchar(50)
,`num_cuota` int(2)
,`fecha_pago` datetime
,`costo` decimal(7,2)
,`estado` varchar(20)
,`color_estado` varchar(20)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_contrato`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_contrato` (
`id` int(11)
,`idcliente` int(11)
,`nombre_razon_cliente` varchar(50)
,`dni` varchar(10)
,`telefono` varchar(15)
,`telefono_adicional` varchar(15)
,`tipo_via` varchar(30)
,`nombre_via` varchar(30)
,`numero_vivienda` varchar(8)
,`tipo_zona` varchar(30)
,`nombre_zona` varchar(30)
,`estado_cliente` tinyint(1)
,`idpaquete` int(11)
,`paquete` varchar(100)
,`precio_mensual` decimal(5,2)
,`megas_bajada` decimal(5,2)
,`megas_subida` decimal(5,2)
,`megas_descarga_comercial` decimal(5,2)
,`megas_subida_comercial` decimal(5,2)
,`fecha_pago` varchar(40)
,`fecha_corte` varchar(40)
,`fecha_inicio_contrato` datetime
,`duracion_contrato` int(2)
,`fecha_fin_contrato` datetime
,`idantena_emisora` int(11)
,`antena_essid` varchar(150)
,`antena_mac` varchar(45)
,`antena_ip` varchar(45)
,`marca_ap` varchar(45)
,`modelo_ap` varchar(45)
,`serie_ap` varchar(45)
,`marca_router` varchar(45)
,`modelo_router` varchar(45)
,`serie_router` varchar(45)
,`costo_instalacion` decimal(7,2)
,`costo_ap` decimal(7,2)
,`tipo_pago_ap` varchar(9)
,`estado` tinyint(1)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_contrato_online`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_contrato_online` (
`id` int(11)
,`idcliente` int(11)
,`nombre_razon_cliente` varchar(50)
,`dni` varchar(10)
,`telefono` varchar(15)
,`telefono_adicional` varchar(15)
,`tipo_via` varchar(30)
,`nombre_via` varchar(30)
,`numero_vivienda` varchar(8)
,`tipo_zona` varchar(30)
,`nombre_zona` varchar(30)
,`departamento` varchar(30)
,`provincia` varchar(30)
,`distrito` varchar(30)
,`referencia` varchar(250)
,`estado_cliente` tinyint(1)
,`idpaquete` int(11)
,`paquete` varchar(100)
,`precio_mensual` decimal(5,2)
,`megas_bajada` decimal(5,2)
,`megas_subida` decimal(5,2)
,`megas_descarga_comercial` decimal(5,2)
,`megas_subida_comercial` decimal(5,2)
,`fecha_pago` varchar(40)
,`fecha_corte` varchar(40)
,`fecha_inicio_contrato` datetime
,`duracion_contrato` int(2)
,`fecha_fin_contrato` datetime
,`marca_ap` varchar(45)
,`modelo_ap` varchar(45)
,`serie_ap` varchar(45)
,`marca_router` varchar(45)
,`modelo_router` varchar(45)
,`serie_router` varchar(45)
,`costo_instalacion` decimal(7,2)
,`costo_ap` decimal(7,2)
,`costo_ap_mensualmente` decimal(7,2)
,`tipo_pago_ap` varchar(9)
,`estado` tinyint(1)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `view_pagos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `view_pagos` (
);

-- --------------------------------------------------------

--
-- Estructura para la vista `view_cobros_pendientes`
--
DROP TABLE IF EXISTS `view_cobros_pendientes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_cobros_pendientes`  AS  select `c`.`idcontrato` AS `idcontrato`,`c`.`estado` AS `contrato_estado`,`m`.`idmensualidad` AS `idmensualidad`,`cl`.`idcliente` AS `idcliente`,`cl`.`nombre_razon` AS `nombre_razon`,`m`.`num_cuota` AS `num_cuota`,`m`.`fecha_pago` AS `fecha_pago`,`m`.`costo` AS `costo`,`m`.`estado` AS `estado`,`m`.`color_estado` AS `color_estado` from ((`mensualidad` `m` join `contrato` `c` on((`m`.`idcontrato` = `c`.`idcontrato`))) join `cliente` `cl` on((`c`.`idcliente` = `cl`.`idcliente`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_contrato`
--
DROP TABLE IF EXISTS `view_contrato`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_contrato`  AS  select `c`.`idcontrato` AS `id`,`cl`.`idcliente` AS `idcliente`,`cl`.`nombre_razon` AS `nombre_razon_cliente`,`cl`.`dni` AS `dni`,`cl`.`telefono` AS `telefono`,`cl`.`telefono_adicional` AS `telefono_adicional`,`cl`.`tipo_via` AS `tipo_via`,`cl`.`nombre_via` AS `nombre_via`,`cl`.`numero_vivienda` AS `numero_vivienda`,`cl`.`tipo_zona` AS `tipo_zona`,`cl`.`nombre_zona` AS `nombre_zona`,`cl`.`estado` AS `estado_cliente`,`p`.`idpaquete` AS `idpaquete`,`p`.`nombre` AS `paquete`,`p`.`precio_mensual` AS `precio_mensual`,`p`.`megas_bajada` AS `megas_bajada`,`p`.`megas_subida` AS `megas_subida`,`p`.`megas_descarga_comercial` AS `megas_descarga_comercial`,`p`.`megas_subida_comercial` AS `megas_subida_comercial`,`c`.`fecha_pago_servicio` AS `fecha_pago`,`c`.`fecha_corte` AS `fecha_corte`,`c`.`fecha_inicio_contrato` AS `fecha_inicio_contrato`,`c`.`duracion_contrato` AS `duracion_contrato`,`c`.`fecha_fin_contrato` AS `fecha_fin_contrato`,`a`.`idantena_emisora` AS `idantena_emisora`,`a`.`essid` AS `antena_essid`,`a`.`mac` AS `antena_mac`,`a`.`ip` AS `antena_ip`,`c`.`marca_ap` AS `marca_ap`,`c`.`modelo_ap` AS `modelo_ap`,`c`.`serie_ap` AS `serie_ap`,`c`.`marca_router` AS `marca_router`,`c`.`modelo_router` AS `modelo_router`,`c`.`serie_router` AS `serie_router`,`c`.`costo_instalacion` AS `costo_instalacion`,`c`.`costo_ap` AS `costo_ap`,`c`.`tipo_pago_ap` AS `tipo_pago_ap`,`c`.`estado` AS `estado` from (((`contrato` `c` join `cliente` `cl` on((`c`.`idcliente` = `cl`.`idcliente`))) join `paquete` `p` on((`c`.`idpaquete` = `p`.`idpaquete`))) join `antena_emisora` `a` on((`c`.`idantena_emisora` = `a`.`idantena_emisora`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_contrato_online`
--
DROP TABLE IF EXISTS `view_contrato_online`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_contrato_online`  AS  select `c`.`idcontrato` AS `id`,`cl`.`idcliente` AS `idcliente`,`cl`.`nombre_razon` AS `nombre_razon_cliente`,`cl`.`dni` AS `dni`,`cl`.`telefono` AS `telefono`,`cl`.`telefono_adicional` AS `telefono_adicional`,`cl`.`tipo_via` AS `tipo_via`,`cl`.`nombre_via` AS `nombre_via`,`cl`.`numero_vivienda` AS `numero_vivienda`,`cl`.`tipo_zona` AS `tipo_zona`,`cl`.`nombre_zona` AS `nombre_zona`,`cl`.`departamento` AS `departamento`,`cl`.`provincia` AS `provincia`,`cl`.`distrito` AS `distrito`,`cl`.`referencia_direccion` AS `referencia`,`cl`.`estado` AS `estado_cliente`,`p`.`idpaquete` AS `idpaquete`,`p`.`nombre` AS `paquete`,`p`.`precio_mensual` AS `precio_mensual`,`p`.`megas_bajada` AS `megas_bajada`,`p`.`megas_subida` AS `megas_subida`,`p`.`megas_descarga_comercial` AS `megas_descarga_comercial`,`p`.`megas_subida_comercial` AS `megas_subida_comercial`,`c`.`fecha_pago_servicio` AS `fecha_pago`,`c`.`fecha_corte` AS `fecha_corte`,`c`.`fecha_inicio_contrato` AS `fecha_inicio_contrato`,`c`.`duracion_contrato` AS `duracion_contrato`,`c`.`fecha_fin_contrato` AS `fecha_fin_contrato`,`c`.`marca_ap` AS `marca_ap`,`c`.`modelo_ap` AS `modelo_ap`,`c`.`serie_ap` AS `serie_ap`,`c`.`marca_router` AS `marca_router`,`c`.`modelo_router` AS `modelo_router`,`c`.`serie_router` AS `serie_router`,`c`.`costo_instalacion` AS `costo_instalacion`,`c`.`costo_ap` AS `costo_ap`,`c`.`costo_ap_mensualmente` AS `costo_ap_mensualmente`,`c`.`tipo_pago_ap` AS `tipo_pago_ap`,`c`.`estado` AS `estado` from ((`contrato` `c` join `cliente` `cl` on((`c`.`idcliente` = `cl`.`idcliente`))) join `paquete` `p` on((`c`.`idpaquete` = `p`.`idpaquete`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `view_pagos`
--
DROP TABLE IF EXISTS `view_pagos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pagos`  AS  select `pm`.`idpago_mensualidad` AS `idpago_mensualidad`,`c`.`idcliente` AS `idcliente`,`c`.`nombre` AS `nombre`,`c`.`apellido` AS `apellido`,`c`.`dni` AS `dni`,`c`.`telefono` AS `telefono`,`c`.`tipo_via` AS `tipo_via`,`c`.`nombre_via` AS `nombre_via`,`c`.`numero_vivienda` AS `numero_vivienda`,`c`.`tipo_zona` AS `tipo_zona`,`c`.`nombre_zona` AS `nombre_zona`,`pm`.`fecha_hora_pagado` AS `fecha_hora_pagado`,`pm`.`tipo_comprobante` AS `tipo_comprobante`,`pm`.`serie_comprobante` AS `serie_comprobante`,`pm`.`num_comprobante` AS `num_comprobante`,`m`.`num_cuota` AS `num_cuota`,`m`.`costo` AS `costo`,`m`.`fecha_pago` AS `fecha_pago` from (((`detalle_pago` `dp` join `mensualidad` `m` on((`dp`.`idmensualidad` = `m`.`idmensualidad`))) join `pago_mensualidad` `pm` on((`dp`.`idpago_mensualidad` = `pm`.`idpago_mensualidad`))) join `cliente` `c` on((`pm`.`idcliente` = `c`.`idcliente`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `antena_emisora`
--
ALTER TABLE `antena_emisora`
  ADD PRIMARY KEY (`idantena_emisora`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`idcontrato`),
  ADD KEY `idcliente_idx` (`idcliente`),
  ADD KEY `idpaquete_idx` (`idpaquete`),
  ADD KEY `idantena_emisora_idx` (`idantena_emisora`);

--
-- Indices de la tabla `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currencies_code_index` (`code`);

--
-- Indices de la tabla `detalle_pago`
--
ALTER TABLE `detalle_pago`
  ADD PRIMARY KEY (`iddetalle_pago`),
  ADD KEY `idpago_mensualidad_idx` (`idpago_mensualidad`),
  ADD KEY `idmensualidad_idx` (`idmensualidad`);

--
-- Indices de la tabla `mensualidad`
--
ALTER TABLE `mensualidad`
  ADD PRIMARY KEY (`idmensualidad`),
  ADD KEY `idcontrato_idx` (`idcontrato`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago_mensualidad`
--
ALTER TABLE `pago_mensualidad`
  ADD PRIMARY KEY (`idpago_mensualidad`),
  ADD KEY `fk_pago_mensualidad_cliente1_idx` (`idcliente`);

--
-- Indices de la tabla `paquete`
--
ALTER TABLE `paquete`
  ADD PRIMARY KEY (`idpaquete`);

--
-- Indices de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcliente_fk` (`idcliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `antena_emisora`
--
ALTER TABLE `antena_emisora`
  MODIFY `idantena_emisora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `contrato`
--
ALTER TABLE `contrato`
  MODIFY `idcontrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalle_pago`
--
ALTER TABLE `detalle_pago`
  MODIFY `iddetalle_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `mensualidad`
--
ALTER TABLE `mensualidad`
  MODIFY `idmensualidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=386;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `pago_mensualidad`
--
ALTER TABLE `pago_mensualidad`
  MODIFY `idpago_mensualidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `paquete`
--
ALTER TABLE `paquete`
  MODIFY `idpaquete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `idantena_emisora` FOREIGN KEY (`idantena_emisora`) REFERENCES `antena_emisora` (`idantena_emisora`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idcliente` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idpaquete` FOREIGN KEY (`idpaquete`) REFERENCES `paquete` (`idpaquete`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_pago`
--
ALTER TABLE `detalle_pago`
  ADD CONSTRAINT `idmensualidad` FOREIGN KEY (`idmensualidad`) REFERENCES `mensualidad` (`idmensualidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idpago_mensualidad` FOREIGN KEY (`idpago_mensualidad`) REFERENCES `pago_mensualidad` (`idpago_mensualidad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mensualidad`
--
ALTER TABLE `mensualidad`
  ADD CONSTRAINT `idcontrato` FOREIGN KEY (`idcontrato`) REFERENCES `contrato` (`idcontrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pago_mensualidad`
--
ALTER TABLE `pago_mensualidad`
  ADD CONSTRAINT `fk_pago_mensualidad_cliente` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `idcliente_fk` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
